package com.project.dmitry.amortcar.addOutlayActivityPac;

import android.content.Context;
import android.content.Intent;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.TextView;


import com.project.dmitry.amortcar.mainActivityPac.MainActivity;
import com.project.dmitry.amortcar.models.Car;
import com.project.dmitry.amortcar.models.Plan;
import com.project.dmitry.amortcar.models.Type;
import com.project.dmitry.amortcar.MySugarApp;
import com.project.dmitry.amortcar.utils.DatePickerFragment;
import com.project.dmitry.amortcar.utils.DatePickerFragmentListener;
import com.project.dmitry.amortcar.R;
import com.project.dmitry.amortcar.utils.InputFormatException;
import com.project.dmitry.amortcar.utils.InputValidator;

import java.util.Calendar;
import java.util.Iterator;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AddOutlayActivity extends AppCompatActivity implements DatePickerFragmentListener {
    @BindView(R.id.ac_add_outlay_tipe_autocomplete)
    AutoCompleteTextView typeAutocomplete;
    @BindView(R.id.ac_add_outlay_type_input_layout)
    TextInputLayout planTypeInputLayout;

    @BindView(R.id.ac_add_outlay_price_input)
    TextView priseInputView;
    @BindView(R.id.ac_add_outlay_time_input)
    TextView timeInputView;

    @BindView(R.id.ac_add_outlay_price_label)
    TextView priceLabel;
    @BindView(R.id.ac_add_outlay_time_label)
    TextView timeLabel;

    @BindView(R.id.ac_add_outlay_saveBtn)
    Button saveBtn;

    Calendar date;
    Car car=null;

    @Override
    protected void onResume() {
        super.onResume();
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_outlay);
        ButterKnife.bind(this);

        car = MySugarApp.getSelectedCar();
        if (car == null) {
            throw new IllegalStateException("PlanAc no car");
        }

        //      types--------------------------------------
        ArrayAdapter<String> adapter = new ArrayAdapter<>(
                this, android.R.layout.simple_dropdown_item_1line);
        Iterator<Type> typeIterator = Type.findAll(Type.class);
        while (typeIterator.hasNext()) {
            adapter.add(typeIterator.next().getName());
        }
        typeAutocomplete.setAdapter(adapter);
        typeAutocomplete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View arg0) {
                typeAutocomplete.showDropDown();
            }
        });

//      types--------------------------------------
//      Date---------------------------------------
        Calendar c = Calendar.getInstance();
        onDateSet(c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH), null);
        timeInputView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DatePickerFragment newFragment = new DatePickerFragment();
                newFragment.setDatePickerListener(AddOutlayActivity.this);
                newFragment.show(getSupportFragmentManager(), "datePicker");
            }
        });
//      Date---------------------------------------
        saveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Plan p = checkInputs();
                if (p != null) {
                    p.closePlanInEndDate();
                    p.save();
                    MainActivity.start(AddOutlayActivity.this);
                }
            }
        });
    }
    private Plan checkInputs() {

        String price;
        double parsedPrice = 0;
        String type;
        boolean isErrorInput = false;
        Type nowType = null;

        price = priseInputView.getText().toString();
        type = typeAutocomplete.getText().toString();

        //delete errorsMessage
        planTypeInputLayout.setErrorEnabled(false);

        priceLabel.setTextColor(getResources().getColor(android.R.color.black));
        timeLabel.setTextColor(getResources().getColor(android.R.color.black));

        //get Common Fileld Values
        if (type.equals("")) {
            planTypeInputLayout.setError(getString(R.string.empty_required_field));
            isErrorInput = true;
        }else {
            List<Type> t = Type.find(Type.class, "name=?", new String[]{type}, null, null, "1");
            if (t.isEmpty()) {
                nowType = new Type(type);
                nowType.save();
            } else {
                nowType = t.get(0);
            }
        }

        try {
            parsedPrice = InputValidator.doubleDecode(price);
        } catch (InputFormatException e) {
            priceLabel.setTextColor(getResources().getColor(android.R.color.holo_red_light));
            isErrorInput = true;
        }
        if(!isErrorInput) {
            return new Plan(car, nowType, parsedPrice, date, date, null, null);
        }

        return null;
    }

    @Override
    public void onDateSet(int year, int month, int day, String tag) {
        timeInputView.setText(getResources().getString(R.string.date_text_template, day, month, year));
        Calendar c = Calendar.getInstance();
        c.set(year, month, day);
        date = c;
    }
    public static void start(Context ctx) {
        Intent in = new Intent(ctx, AddOutlayActivity.class);
        ctx.startActivity(in);
    }
}
