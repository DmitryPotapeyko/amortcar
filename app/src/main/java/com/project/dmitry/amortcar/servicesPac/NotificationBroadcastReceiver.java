package com.project.dmitry.amortcar.servicesPac;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v4.app.NotificationCompat;

import com.project.dmitry.amortcar.mainActivityPac.MainActivity;
import com.project.dmitry.amortcar.R;
import com.project.dmitry.amortcar.settingsActivityPac.TimePreference;

import java.util.Calendar;

import static android.content.Context.NOTIFICATION_SERVICE;

public class NotificationBroadcastReceiver extends BroadcastReceiver {
    private static final int NOTIFICATION_ID = 121241;
    private SharedPreferences sPref;
    public static final String NEXT_NOTIFICATION_TIME = "NEXT_NOTIFICATION_TIME";
    public static final String NEXT_WEEK_NOTIFICATION_TIME = "NEXT_WEEK_NOTIFICATION_TIME";

    private static final String periodKey = "period_list";
    private static final String timeKey = "notification_time";
    private static final String dayKey = "day_list";
    int time;

    public static void onNotificationSettingsChanged(@NonNull Context context) {
        SharedPreferences sPref = PreferenceManager.getDefaultSharedPreferences(context);
        String period = sPref.getString(periodKey, "0");
        String time = sPref.getString(timeKey, "9:00");
        String day = sPref.getString(dayKey, "0");
        int hour = TimePreference.getHour(time);
        int minute = TimePreference.getMinute(time);
        Calendar calendar = Calendar.getInstance();
        Calendar cal = Calendar.getInstance();
        calendar.set(Calendar.YEAR, cal.get(Calendar.YEAR));
        calendar.set(Calendar.MONTH, cal.get(Calendar.MONTH));
        if (period.equals("0")) {//every day
            calendar.set(Calendar.DAY_OF_MONTH, cal.get(Calendar.DAY_OF_MONTH));
            calendar.set(Calendar.HOUR_OF_DAY, hour);
            calendar.set(Calendar.MINUTE, minute);
            calendar.set(Calendar.SECOND, 0);
            calendar.set(Calendar.MILLISECOND, 0);
        } else if (period.equals("1")) {
            int dayPars = Integer.decode(day);
            calendar.set(Calendar.DAY_OF_WEEK, dayPars + 1);//days start with 1. 1-sunday ...
            calendar.set(Calendar.HOUR_OF_DAY, hour);
            calendar.set(Calendar.MINUTE, minute);
            calendar.set(Calendar.SECOND, 0);
            calendar.set(Calendar.MILLISECOND, 0);
        }
        final AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent in = new Intent(context, NotificationBroadcastReceiver.class);
        final PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0,
                in, PendingIntent.FLAG_CANCEL_CURRENT);

        if (alarmManager != null) {
            calendar = Calendar.getInstance();
            calendar.add(Calendar.SECOND,30);
            alarmManager.set(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), pendingIntent);
        }

        Calendar planedWeekTime = planedWeeksNotification(context, false);
        SharedPreferences.Editor ed = sPref.edit();
        ed.putLong(NEXT_WEEK_NOTIFICATION_TIME, planedWeekTime.getTimeInMillis());
        ed.putLong(NEXT_NOTIFICATION_TIME, calendar.getTimeInMillis());
        ed.apply();
    }
    public static void onAppStart(@NonNull Context context){
        final AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent in = new Intent(context, NotificationBroadcastReceiver.class);
        final Boolean isAlarmUp = PendingIntent.getBroadcast(context, 0,
                in, PendingIntent.FLAG_NO_CREATE)!=null;
        if(!isAlarmUp){
            onNotificationSettingsChanged(context);
        }
    }

    private static Calendar planedWeeksNotification(@NonNull Context context, boolean onNextWeek) {
        final AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent in = new Intent(context, NotificationBroadcastReceiver.class);
        in.putExtra("Action", "WeeksNotification");
        final PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 1,
                in, PendingIntent.FLAG_CANCEL_CURRENT);
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);
        calendar.set(Calendar.HOUR_OF_DAY, 12);
        calendar.set(Calendar.MINUTE, 0);
        if(onNextWeek) {
            calendar.add(Calendar.DAY_OF_MONTH, 7);
        }
        calendar =Calendar.getInstance();
        calendar.add(Calendar.SECOND,30);
        if (alarmManager != null) {
            alarmManager.set(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), pendingIntent);
        }
        return calendar;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        if (context == null || intent == null) {
            return;
        }
        SharedPreferences sPref = PreferenceManager.getDefaultSharedPreferences(context);
        long nextNotificationTime = sPref.getLong(NEXT_NOTIFICATION_TIME, Calendar.getInstance().getTimeInMillis());
        long nextWeekNotificationTime = sPref.getLong(NEXT_WEEK_NOTIFICATION_TIME, Calendar.getInstance().getTimeInMillis());

        String action = intent.getStringExtra("Action");
        if (action != null && action.equals("WeeksNotification")) {

            Calendar planedWeekTime = planedWeeksNotification(context,true);
            SharedPreferences.Editor ed = sPref.edit();
            ed.putLong(NEXT_WEEK_NOTIFICATION_TIME, planedWeekTime.getTimeInMillis());
            ed.apply();
            if (nextWeekNotificationTime <= Calendar.getInstance().getTimeInMillis()) {
                NotificationService.startActionNotification(context);
            }

        } else {
            Calendar planedNotificationTime = planNextNotification(context,nextNotificationTime);
            SharedPreferences.Editor ed = sPref.edit();
            ed.putLong(NEXT_NOTIFICATION_TIME, planedNotificationTime.getTimeInMillis());
            ed.apply();
            if (nextNotificationTime <= Calendar.getInstance().getTimeInMillis()) {
                Notification nf = getNotification(context, "AmortCar", context.getString(R.string.notif_please_update_text),
                        "", true);
                sendNotification(nf, context, null);
            }
        }
    }

    private Calendar planNextNotification(Context context, long nextNotificationTime) {
        SharedPreferences sPref = PreferenceManager.getDefaultSharedPreferences(context);
        String period = sPref.getString(periodKey, "0");
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(nextNotificationTime);

        if (period.equals("0")) {//every day
            calendar.add(Calendar.DAY_OF_MONTH, 1);
        } else if (period.equals("1")) {
            calendar.add(Calendar.DAY_OF_MONTH, 7);
        }
        final AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent in = new Intent(context, NotificationBroadcastReceiver.class);
        final PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0,
                in, PendingIntent.FLAG_CANCEL_CURRENT);
        if (alarmManager != null) {
            alarmManager.set(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), pendingIntent);
        }
        return calendar;
    }

    static Notification getNotification(Context context, String title, String text, String subText, boolean autoCancel) {
        NotificationCompat.Builder
                builder = new NotificationCompat.Builder(context, "M_CH_ID")
                .setDefaults(Notification.DEFAULT_ALL)
                .setSmallIcon(R.drawable.ic_truck_fast)
                .setContentTitle(title)
                .setContentText(text)
                .setSubText(subText)
                .setAutoCancel(autoCancel)
                .setGroup("AmrtCarGroup");
        //настройка - закрыть уведомление при клике

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder.setCategory(Notification.CATEGORY_ALARM);
        }

        Intent notificationIntent;
        PendingIntent contentIntent;
        if (autoCancel) {
            notificationIntent = new Intent(context, MainActivity.class);
            notificationIntent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            contentIntent = PendingIntent.getActivity(context,
                    0, notificationIntent,
                    PendingIntent.FLAG_CANCEL_CURRENT);

            builder.setContentIntent(contentIntent);//добавление действия
        }
        return builder.build();//создание уведомления
    }

    static void sendNotification(Notification nf, Context context, Integer notification_id) {

        NotificationManager notificationManager = (NotificationManager) context.getSystemService(NOTIFICATION_SERVICE);
        if (notification_id == null) {
            notification_id = NOTIFICATION_ID;
        }
        if (notificationManager != null) {
            notificationManager.notify(notification_id, nf);
        }
    }

}



