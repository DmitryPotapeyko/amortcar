package com.project.dmitry.amortcar.models;

import com.orm.SugarRecord;
import com.orm.dsl.Unique;

import java.util.Calendar;

public class Milage extends SugarRecord {
    @Unique
    private long id;

    public Calendar getDate() {
        return date;
    }

    private Calendar date;
    public int getLength() {
        return length;
    }
    private int length;

    public Car getCar() {
        return car;
    }

    @SuppressWarnings("FieldCanBeLocal")
    private Car car;

    private double accumulatedMoney;

    public double getAccumulatedMoney() {
        return accumulatedMoney;
    }

    public Milage() {
    }

    public Milage(Calendar date, int length, Car car,double sum) {
        this.date = date;
        this.length = length;
        this.car = car;
        this.accumulatedMoney = sum;
    }


}
