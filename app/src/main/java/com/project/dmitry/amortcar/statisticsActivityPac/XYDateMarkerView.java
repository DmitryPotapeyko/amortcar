package com.project.dmitry.amortcar.statisticsActivityPac;

import android.annotation.SuppressLint;
import android.content.Context;
import android.widget.TextView;

import com.github.mikephil.charting.components.MarkerView;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.utils.MPPointF;
import com.project.dmitry.amortcar.R;

import java.util.Calendar;

/**
 * Custom implementation of the MarkerView.
 */
@SuppressLint("ViewConstructor")
public class XYDateMarkerView extends MarkerView {

    private TextView tvContent;
    private TextView tvType;

    public XYDateMarkerView(Context context, int layoutResource) {
        super(context, layoutResource);

        tvContent = (TextView) findViewById(R.id.tvContent);
        tvType = (TextView) findViewById(R.id.tvType);
    }

    // callbacks everytime the MarkerView is redrawn, can be used to update the
    // content (user-interface)
    @Override
    public void refreshContent(Entry e, Highlight highlight) {

        tvContent.setText("" + (int)e.getY() + "км");
        Calendar c = ((Calendar) e.getData());
        tvType.setText(c.get(Calendar.DAY_OF_MONTH)+"."+c.get(Calendar.MONTH)+"."+c.get(Calendar.YEAR)+"  "+
        c.get(Calendar.HOUR_OF_DAY)+":"+c.get(Calendar.MINUTE));

        super.refreshContent(e, highlight);
    }

    @Override
    public MPPointF getOffset() {
        return new MPPointF(-(getWidth() / 2), -getHeight());
    }
}
