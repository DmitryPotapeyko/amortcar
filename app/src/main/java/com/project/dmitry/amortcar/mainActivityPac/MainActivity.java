package com.project.dmitry.amortcar.mainActivityPac;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;

import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.orm.util.NamingHelper;
import com.project.dmitry.amortcar.MySugarApp;
import com.project.dmitry.amortcar.R;
import com.project.dmitry.amortcar.addOutlayActivityPac.AddOutlayActivity;
import com.project.dmitry.amortcar.carActivityPac.AddCarActivity;
import com.project.dmitry.amortcar.milagePac.MilageFragment;
import com.project.dmitry.amortcar.models.Car;
import com.project.dmitry.amortcar.models.Milage;
import com.project.dmitry.amortcar.models.Plan;
import com.project.dmitry.amortcar.planActivityPac.PlanActivity;
import com.project.dmitry.amortcar.servicesPac.NotificationBroadcastReceiver;
import com.project.dmitry.amortcar.settingsActivityPac.SettingsActivity;
import com.project.dmitry.amortcar.startActivity.WelcomeActivity;
import com.project.dmitry.amortcar.statisticsActivityPac.StatisticActivity;
import com.project.dmitry.amortcar.travelCostPac.TravelCoastActivity;
import com.project.dmitry.amortcar.utils.DBImportExport;

import java.util.List;
import java.util.concurrent.ExecutionException;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    private static String CAR_ID_EXTRA_SHARED = "CAR_ID_EXTRA_SHARED";
    private static String STATE_EXTRA_SHARED = "STATE_EXTRA_SHARED";
    private static final String TAG = "MainActivity";

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.ac_main_car_name)
    TextView selectedCarNameTextView;
    @BindView(R.id.ac_main_car_milage)
    TextView selectedCarMilageTextView;
    @BindView(R.id.ac_main_LL3)
    LinearLayout selectedCarInfoView;
    @BindView(R.id.ac_main_car_money)
    TextView selectedCarMoneyTextView;
    @BindView(R.id.ac_main_car_cost_per_km)
    TextView selectedCarCostKmTextView;
    ImageButton addCarBtn;

    private enum State {
        Plans,
        Outlay
    }

    State state = State.Plans;
    private Long selectedCarId;
    private Car selectedCar;
    private List<Car> cars;
    PlansUpdateAsyncTask lastPlansAsyncTask;
    private PlansUpdateAsyncTask.Result result = null;
    private List<Plan> plans;
    private List<Plan> stats;


    private SharedPreferences sharedPreferences;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
//        String appKey = "d21f941cebaece11b957dfa78e939394c7957e09ce36bae2";
//        Appodeal.initialize(this, appKey, Appodeal.INTERSTITIAL | Appodeal.BANNER);
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);

        initSelectedCarAndMilage();
        initState();
        initSubtitle();

        setSupportActionBar(toolbar);

        initFloatingActionButton();


        selectedCarInfoView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (selectedCar != null) {
                    startMilageFragmentForCar();
                }
            }
        });

        initNavigationView();

        if (state == State.Outlay) {
            initStatFragment();
        } else {
            initPlansFragment();
        }
    }

    private void initSubtitle() {
        String subtitle;
        switch (state) {
            case Plans:
                subtitle = getResources().getString(R.string.plan_subtitle);
                break;
            case Outlay:
                subtitle = getResources().getString(R.string.outlay_subtitle);
                break;
            default:
                subtitle = "";
        }
        toolbar.setSubtitle(subtitle);
    }

    private void initState() {
        state = State.values()[sharedPreferences.getInt(STATE_EXTRA_SHARED, State.Plans.ordinal())];
    }

    private void initFloatingActionButton() {
        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent in = new Intent(view.getContext(), NotificationBroadcastReceiver.class);
                sendBroadcast(in);
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
//                if (state == State.Plans) {
//                    PlanActivity.start(MainActivity.this);
//                } else if (state == State.Outlay) {
//                    AddOutlayActivity.start(MainActivity.this);
//                }
            }
        });
        if (selectedCar == null) {
            fab.hide();
        } else {
            fab.show();
        }
    }

    private void initPlansFragment() {
        selectedCarMoneyTextView.setVisibility(View.VISIBLE);
        selectedCarCostKmTextView.setVisibility(View.VISIBLE);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        PlansRecyclerViewFragment plansFragment = new PlansRecyclerViewFragment();
        initPlansDataset();
        plansFragment.setDataSet(this.plans);
        transaction.replace(R.id.ac_main_fragments_container, plansFragment);
        transaction.commit();
    }

    private void initStatFragment() {
        selectedCarMoneyTextView.setVisibility(View.INVISIBLE);
        selectedCarCostKmTextView.setVisibility(View.INVISIBLE);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        StatRecyclerViewFragment statFragment = new StatRecyclerViewFragment();
        initStatsDataSet();
        statFragment.setDataSet(this.stats);
        transaction.replace(R.id.ac_main_fragments_container, statFragment);
        transaction.commit();
    }

    private void initStatsDataSet() {
        if (MySugarApp.getSelectedCar() != null) {
            stats = Plan.find(Plan.class, "car=? and CLOSE_STATUS=?", new String[]{
                            String.valueOf(MySugarApp.getSelectedCar().getId()), "1"}, null,
                    "END_DATE desc, id", null);
        } else {
            stats = null;
        }
    }

    private void initSelectedCarAndMilage() {
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        selectedCar = MySugarApp.getSelectedCar();
        if (selectedCar == null) {
            selectedCarId = sharedPreferences.getLong(CAR_ID_EXTRA_SHARED, -1L);
            selectedCar = Car.findById(Car.class, selectedCarId);
            MySugarApp.setSelectedCar(selectedCar);
        } else {
            selectedCarId = selectedCar.getId();
        }
        Milage lastMilage = MySugarApp.getLastMilageOfSelectedCar();
        if (lastMilage == null) {
            List<Milage> l = Milage.find(Milage.class, "car=?", new String[]{"" + selectedCarId}, null, "id desc", "1");
            if (l.isEmpty()) {
                MySugarApp.setLastMilageOfSelectedCar(null);
            } else {
                lastMilage = l.get(0);
                MySugarApp.setLastMilageOfSelectedCar(lastMilage);
            }
        }
    }

    private void sharedSetSelectedId(long selectedCarId) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putLong(CAR_ID_EXTRA_SHARED, selectedCarId);
        editor.apply();
    }

    private void sharedSetState(State state) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(STATE_EXTRA_SHARED, state.ordinal());
        editor.apply();
    }

    private void initNavigationView() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        NavigationView navView = findViewById(R.id.nav_view);
        addCarBtn = navView.getHeaderView(0).findViewById(R.id.add_car);
        addCarBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AddCarActivity.start(MainActivity.this);
            }
        });

        addNavMenuItems();
        MenuItem privacyItem = navView.getMenu().findItem(R.id.privacy_policy);
        Uri address = Uri.parse("https://sites.google.com/view/amortcar/main"); // TODO: 03.02.2019
        final Intent openLinkIntent = new Intent(Intent.ACTION_VIEW, address);
        privacyItem.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                try {
                    startActivity(openLinkIntent);
                }catch (Throwable e){
                    Toast.makeText(MainActivity.this, "Please install a web browser or " +
                                    "https://sites.google.com/view/amortcar/main",
                            Toast.LENGTH_LONG).show();
                }
                return true;
            }
        });
    }

    private void addNavMenuItems() {
        NavigationView navigationView = findViewById(R.id.nav_view);
        Menu navMenu = navigationView.getMenu();
        cars = Car.listAll(Car.class);
        SubMenu subMenu = navMenu.addSubMenu(R.string.ac_main_nav_added_auto);

        String title;
        boolean checkedFlag;
        for (int i = 0; i < cars.size(); i++) {
            Car c = cars.get(i);
            title = c.getTitle();


            checkedFlag = c.getId().equals(selectedCarId);
            subMenu.add(R.id.cars_group, i, Menu.NONE, title)
                    .setCheckable(true)
                    .setChecked(checkedFlag)
                    .setIcon(R.drawable.ic_directions_car_black_24dp);
            if (checkedFlag) {
                initSelectedCarSubtitle(title);
            }
            navigationView.setNavigationItemSelectedListener(this);
        }
        subMenu.setGroupCheckable(R.id.cars_group, true, true);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        if (MySugarApp.getSelectedCar() == null) {
            menu.findItem(R.id.action_auto).setEnabled(false);
        } else {
            menu.findItem(R.id.action_auto).setEnabled(true);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.action_cost: {
                if(selectedCar!=null) {
                    TravelCoastActivity.start(MainActivity.this);
                }else{
                    Toast.makeText(this,getString(R.string.select_car_text),Toast.LENGTH_SHORT).show();
                }
                break;
            }
            case R.id.action_settings: {
                Intent intent = new Intent(this, SettingsActivity.class);
                this.startActivity(intent);
                break;
            }
            case R.id.action_auto: {
                if(selectedCar!=null) {
                    AddCarActivity.start(this, true);
                }
                break;
            }
            case R.id.action_welcome: {
                WelcomeActivity.start(this, true);
                break;
            }
            case R.id.action_export: {
                DBImportExport.start(this);
                break;
            }
        }
        return super.onOptionsItemSelected(item);
    }

    private void startMilageFragmentForCar() {
        MilageFragment milage = new MilageFragment();
        milage.setAccumulatedMoney(0);
        if (result != null) {
            milage.setAccumulatedMoney(result.getMoney());
        } else {
            if (lastPlansAsyncTask != null && !lastPlansAsyncTask.isCancelled()) {
                try {
                    double res = lastPlansAsyncTask.get().getMoney();
                    milage.setAccumulatedMoney(res);
                } catch (InterruptedException | ExecutionException e) {
                    e.printStackTrace();
                }
            }
        }
        milage.setResultListener(new MilageFragment.ResultListener() {
            @Override
            public void onResult() {
                initSelectedCarSubtitle(null);
                refreshCurrentFragment(state);
            }
        });
        milage.show(getSupportFragmentManager(), "Milage");

    }

    @Override
    protected void onStop() {
        super.onStop();
        if (selectedCarId != null) {
            sharedSetSelectedId(selectedCarId);
        }
        sharedSetState(state);
        if (lastPlansAsyncTask != null && !lastPlansAsyncTask.isCancelled()) {
            lastPlansAsyncTask.cancel(true);
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        // Handle navigation view item clicks here.
        int groupId = item.getGroupId();
        int id = item.getItemId();
        if (groupId == R.id.cars_group) {
            selectedCarId = cars.get(id).getId();
            MySugarApp.setSelectedCar(cars.get(id));
            selectedCar = MySugarApp.getSelectedCar();
            List<Milage> milage = Milage.find(Milage.class, "car=?", new String[]{"" + selectedCarId}, null, "id desc", "1");
            if (!milage.isEmpty()) {
                MySugarApp.setLastMilageOfSelectedCar(milage.get(0));
            } else {
                MySugarApp.setLastMilageOfSelectedCar(null);
            }
            initSelectedCarSubtitle(item.getTitle().toString());
            refreshCurrentFragment(state);
        }

        if (id == R.id.nav_plans) {
            if (state != State.Plans) {
                state = State.Plans;
                initSubtitle();
                initPlansFragment();
            }

        }
        if (id == R.id.nav_outlay) {
            if (state != State.Outlay) {
                state = State.Outlay;
                initSubtitle();
                initStatFragment();
            }
        }
        if (id == R.id.nav_statistic) {
            if(selectedCar!=null){
            StatisticActivity.start(this);
            }else{
                Toast.makeText(this,getString(R.string.select_car_text),Toast.LENGTH_SHORT).show();
            }
        }
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void refreshCurrentFragment(State state) {
        Fragment fr;
        if (state == State.Plans) {
            PlansRecyclerViewFragment plansFragment =
                    (PlansRecyclerViewFragment) getSupportFragmentManager().findFragmentById(R.id.ac_main_fragments_container);
            initPlansDataset();
            plansFragment.setDataSet(this.plans);
            fr = plansFragment;
        } else {
            StatRecyclerViewFragment statFragment =
                    (StatRecyclerViewFragment) getSupportFragmentManager().findFragmentById(R.id.ac_main_fragments_container);
            initStatsDataSet();
            statFragment.setDataSet(this.stats);
            fr = statFragment;
        }
        FragmentTransaction tr = getSupportFragmentManager().beginTransaction();
        tr.detach(fr);
        tr.attach(fr);
        tr.commit();
    }

    private void initSelectedCarSubtitle(String newTitle) {
        if (selectedCarId != null) {
            if (newTitle != null) {
                selectedCarNameTextView.setText(newTitle);
            }
            if (MySugarApp.getLastMilageOfSelectedCar() != null) {
                selectedCarMilageTextView.setText(MySugarApp.getLastMilageOfSelectedCar().getLength() + " км");
                //установка цены за км и накопленных денег.

            } else {
                selectedCarMilageTextView.setText("0 км");
            }
        }
    }


    public static void start(Context ctx) {
        Intent intent = new Intent(ctx, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        ctx.startActivity(intent);
    }

    public void startPlansAsyncTask() {
        if (lastPlansAsyncTask != null) {
            lastPlansAsyncTask.cancel(true);
        }
        lastPlansAsyncTask = new PlansUpdateAsyncTask(this);
        //noinspection unchecked
        lastPlansAsyncTask.execute(plans);
    }

    private void initPlansDataset() {
        if (MySugarApp.getSelectedCar() != null) {
            plans = Plan.find(Plan.class, "car=? and CLOSE_STATUS=?", new String[]{
                            String.valueOf(MySugarApp.getSelectedCar().getId()), "0"}, null,
                    NamingHelper.toSQLNameDefault("percentOfPassed") + " desc, id", null);
            startPlansAsyncTask();
        } else {
            plans = null;
        }
    }

    protected void onPlansUpdateAsyncTaskResult(PlansUpdateAsyncTask.Result result) {
        if (result != null) {
            this.result = result;
            selectedCarMoneyTextView.setText(MySugarApp.getApplicationResources().getString(
                    R.string.ac_main_all_money_text_template, result.getMoney(), "\u20BD"));//₽ - знак рубля
            selectedCarCostKmTextView.setText(MySugarApp.getApplicationResources().getString(
                    R.string.ac_main_cost_per_km_text_template, result.getCostPerKm(), "\u20BD", "км"));
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }
}
