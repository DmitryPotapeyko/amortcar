package com.project.dmitry.amortcar.travelCostPac;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.project.dmitry.amortcar.MySugarApp;
import com.project.dmitry.amortcar.R;
import com.project.dmitry.amortcar.models.Plan;
import com.project.dmitry.amortcar.utils.InputFormatException;
import com.project.dmitry.amortcar.utils.InputValidator;

import java.util.Calendar;
import java.util.List;
import java.util.concurrent.Callable;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

public class TravelCoastActivity extends AppCompatActivity {
    private static String COST_EXTRA = "COST_EXTRA";
    private static String STATE = "STATE_INSTANT";
    private static String COST = "COST_INSTANT";

    private enum State {
        inInput,
        inProgress,
        inResult
    }

    Double amortCost = null;
    int lengthPars = 0;
    double costPars = 0;
    double fuelRatePars = 0;
    Disposable disposable = null;
    State state = State.inInput;
    @BindView(R.id.ac_travel_cost_input_view)
    ConstraintLayout inputLayout;

    @BindView(R.id.ac_travel_cost_length_input_layout)
    TextInputLayout lengthTextInputLayout;
    @BindView(R.id.ac_travel_cost_length_input)
    TextView lengthInput;

    @BindView(R.id.ac_travel_cost_cost_input_layout)
    TextInputLayout costTextInputLayout;
    @BindView(R.id.ac_travel_cost_cost_input)
    TextView costInput;


    @BindView(R.id.ac_travel_cost_fuel_rate_input_layout)
    TextInputLayout fuelRateTextInputLayout;
    @BindView(R.id.ac_travel_cost_fuel_rate_input)
    TextView fuelRateInput;


    @BindView(R.id.ac_travel_cost_progress_view)
    LinearLayout progressLayout;


    @BindView(R.id.ac_travel_cost_result_view)
    LinearLayout resultLayout;

    @BindView(R.id.ac_travel_cost_result_dop1)
    TextView resultTextViewFuelDop;
    @BindView(R.id.ac_travel_cost_result_dop2)
    TextView resultTextViewPlansDop;
    @BindView(R.id.ac_travel_cost_result)
    TextView resultTextView;


    @OnClick(R.id.ac_travel_cost_calcBtn)
    void onCalculateClick() {
        boolean isErrorInput = false;
        lengthInput.clearFocus();
        costInput.clearFocus();
        fuelRateInput.clearFocus();

        InputMethodManager imm = (InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm != null && getCurrentFocus() != null) {
            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
        String length = lengthInput.getText().toString();
        String cost = costInput.getText().toString();
        String fuelRate = fuelRateInput.getText().toString();

        lengthTextInputLayout.setErrorEnabled(false);
        costTextInputLayout.setErrorEnabled(false);
        fuelRateTextInputLayout.setErrorEnabled(false);

        if (length.equals("")) {
            lengthTextInputLayout.setError(getString(R.string.empty_required_field));
            isErrorInput = true;
        } else {
            try {
                lengthPars = InputValidator.integerDecode(length);
            } catch (InputFormatException e) {
                lengthTextInputLayout.setError(this.getString(R.string.format_error_text));
                isErrorInput = true;
            }
        }
        if (cost.equals("")) {
            costTextInputLayout.setError(getString(R.string.empty_required_field));
            isErrorInput = true;
        } else {
            try {
                costPars = InputValidator.doubleDecode(cost);
            } catch (InputFormatException e) {
                costTextInputLayout.setError(this.getString(R.string.format_error_text));
                isErrorInput = true;
            }
        }
        if (fuelRate.equals("")) {
            fuelRateTextInputLayout.setError(getString(R.string.empty_required_field));
            isErrorInput = true;
        } else {
            try {
                fuelRatePars = InputValidator.doubleDecode(fuelRate);
            } catch (InputFormatException e) {
                fuelRateTextInputLayout.setError(this.getString(R.string.format_error_text));
                isErrorInput = true;
            }
        }
        if (!isErrorInput) {
            inputLayout.setVisibility(View.INVISIBLE);
            progressLayout.setVisibility(View.VISIBLE);
            state = State.inProgress;

            Single<Double> method = Single.fromCallable(
                    new Callable<Double>() {
                        @Override
                        public Double call() throws Exception {
                            return calculate();
                        }
                    })
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread());


            disposable = method.subscribeWith(new DisposableSingleObserver<Double>() {
                @Override
                public void onSuccess(Double aDouble) {
                    showResult(aDouble);
                    state = State.inResult;
                    if (disposable != null) {
                        disposable.dispose();
                    }
                }

                @Override
                public void onError(Throwable e) {
                    inputLayout.setVisibility(View.VISIBLE);
                    progressLayout.setVisibility(View.INVISIBLE);
                    resultLayout.setVisibility(View.INVISIBLE);
                    Toast.makeText(TravelCoastActivity.this, "Error", Toast.LENGTH_LONG).show();
                    state = State.inInput;
                    if (disposable != null) {
                        disposable.dispose();
                    }
                }
            });
        }
    }

    private double calculate() {
        double res = costPars * fuelRatePars * lengthPars / 100;
        final List<Plan> plans = Plan.find(Plan.class, "car=? and CLOSE_STATUS=?",
                String.valueOf(MySugarApp.getSelectedCar().getId()), "0");
        double money = 0;
        for (Plan p : plans) {
            money += p.getDopMoneyWithoutUpdateIn(lengthPars, Calendar.getInstance());
        }
        return res + money;
    }

    private void showResult(double res) {
        inputLayout.setVisibility(View.INVISIBLE);
        progressLayout.setVisibility(View.INVISIBLE);
        resultLayout.setVisibility(View.VISIBLE);
        double fuelCost = costPars * fuelRatePars * lengthPars / 100;
        resultTextView.setText(this.getString(R.string.money_double_text_template, res, "\u20BD"));
        resultTextViewFuelDop.setText(this.getString(R.string.travel_cost_fuel_dop, fuelCost));
        resultTextViewPlansDop.setText(this.getString(R.string.travel_cost_plans_dop, res - fuelCost));
    }


    @Override
    public void onBackPressed() {
        onBack();

    }

    private void onBack() {
        switch (state) {
            case inInput:
                super.onBackPressed();
                break;
            case inProgress: {
                inputLayout.setVisibility(View.VISIBLE);
                progressLayout.setVisibility(View.INVISIBLE);
                //stopCalc
                state = State.inInput;
                break;
            }
            case inResult: {
                resultLayout.setVisibility(View.INVISIBLE);
                inputLayout.setVisibility(View.VISIBLE);
                state = State.inInput;
                break;
            }
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable(STATE, state);
        if (amortCost != null) {
            outState.putDouble(COST, amortCost);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_travel_coast);
        ButterKnife.bind(this);
        Intent startIntent = getIntent();
        if (savedInstanceState != null) {
            State st = (State) savedInstanceState.getSerializable(STATE);
            if (st != null) state = st;
            double c = savedInstanceState.getDouble(COST, -1);
            if (c != -1) {
                amortCost = c;
            }
        }
        resultLayout.setVisibility(View.INVISIBLE);
        inputLayout.setVisibility(View.INVISIBLE);
        progressLayout.setVisibility(View.INVISIBLE);
        switch (state) {
            case inResult:
                resultLayout.setVisibility(View.VISIBLE);
                break;
            case inProgress:
                progressLayout.setVisibility(View.VISIBLE);
                break;
            case inInput:
                inputLayout.setVisibility(View.VISIBLE);
                break;
        }
    }

    public static void start(@NonNull final Activity activity) {
        Intent intent = new Intent(activity, TravelCoastActivity.class);
        activity.startActivity(intent);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBack();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
