package com.project.dmitry.amortcar.mainActivityPac;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.TextView;

import com.project.dmitry.amortcar.models.Plan;
import com.project.dmitry.amortcar.MySugarApp;
import com.project.dmitry.amortcar.R;
import com.project.dmitry.amortcar.utils.MLog;

import java.util.Calendar;
import java.util.List;

/**
 * Provide views to RecyclerView with data from mDataSet.
 */
public class StatCustomAdapter extends RecyclerView.Adapter<StatCustomAdapter.ViewHolder> {
    private static final String TAG = "StatCustomAdapter";
    private List<Plan> mDataSet;
    /**
     * Provide a reference to the type of views that you are using (custom ViewHolder)
     */
    class ViewHolder extends RecyclerView.ViewHolder {
        private final TextView typeTextView, dateTextView, moneyTextView;
        private final CardView thisCardView;


        ViewHolder(CardView v) {
            super(v);
            // Define click listener for the ViewHolder's View.
            thisCardView = v;
            typeTextView = v.findViewById(R.id.stat_card_type);
            dateTextView = v.findViewById(R.id.stat_card_date);
            moneyTextView = v.findViewById(R.id.stat_card_money);
        }

//        private void showPopupMenu(final View itemView, final View v, final int layoutPosition) {
//            PopupMenu popupMenu = new PopupMenu(itemView.getContext(), v);
//            popupMenu.inflate(R.menu.popap_in_plans);
//            popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
//
//                @Override
//                public boolean onMenuItemClick(MenuItem item) {
//                    // Toast.makeText(PopupMenuDemoActivity.this,
//                    // item.toString(), Toast.LENGTH_LONG).show();
//                    // return true;
//                    switch (item.getItemId()) {
//                        case R.id.menu1:
//
//                            MyAlertDialogsMaker.getSimpleAlertDialog(itemView.getContext(),
//                                    "Завершение!",
//                                    "Вы уверены, что хотите завершить план? Он будет переведен в выполненные" +
//                                            "задания и не будет учитываться в планировании в дальейшем!",
//                                    "Удалить",
//                                    new DialogInterface.OnClickListener() {
//                                        @Override
//                                        public void onClick(DialogInterface dialog, int which) {
//                                            Plan p = mDataSet.get(layoutPosition);
//                                            p.closePlan();
//                                            p.update();
//                                            mDataSet.remove(layoutPosition);
//                                            notifyItemRemoved(layoutPosition);
//                                            notifyItemRangeChanged(layoutPosition, mDataSet.size());
//                                            notifyDataSetChanged();
//                                        }
//                                    },
//                                    "Отмена!",
//                                    new DialogInterface.OnClickListener() {
//                                        @Override
//                                        public void onClick(DialogInterface dialog, int which) {
//
//                                        }
//                                    }).show();
//                            return true;
//
//
////                        case R.id.menu2:
//
////                            return true;
//                        case R.id.menu3: {
//                            MyAlertDialogsMaker.getSimpleAlertDialog(itemView.getContext(),
//                                    "Удаление!",
//                                    "Вы уверены, что хотите удалить элемент? Он будет удален безвозвратно!",
//                                    "Удалить",
//                                    new DialogInterface.OnClickListener() {
//                                        @Override
//                                        public void onClick(DialogInterface dialog, int which) {
//                                            Plan p = mDataSet.get(layoutPosition);
//                                            p.delete();
//                                            mDataSet.remove(layoutPosition);
//                                            notifyItemRemoved(layoutPosition);
//                                            notifyItemRangeChanged(layoutPosition, mDataSet.size());
//                                            notifyDataSetChanged();
//                                        }
//                                    },
//                                    "Отмена!",
//                                    new DialogInterface.OnClickListener() {
//                                        @Override
//                                        public void onClick(DialogInterface dialog, int which) {
//
//                                        }
//                                    }).show();
//                            return true;
//                        }
//
//
//
//                    }
//                    return false;
//                }
//            });
//
//            popupMenu.show();
//        }
    }

    /**
     * Initialize the dataset of the Adapter.
     *
     * @param dataSet List<Plan> containing the data to populate views to be used by RecyclerView.
     */
    public StatCustomAdapter(List<Plan> dataSet) {
        mDataSet = dataSet;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        // Create a new view.
        CardView v = (CardView) LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.stat_card_view, viewGroup, false);
        return new ViewHolder(v);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {
        MLog.log(TAG, "Element " + position + " set.");

        // Get element from your dataset at this position and replace the contents of the view
        // with that element
        ViewGroup.MarginLayoutParams params =
                (ViewGroup.MarginLayoutParams) viewHolder.thisCardView.getLayoutParams();
        if (position + 1 == getItemCount()) {
            params.setMargins(MySugarApp.getPixelsFromDPs(8), MySugarApp.getPixelsFromDPs(8), MySugarApp.getPixelsFromDPs(8), MySugarApp.getPixelsFromDPs(80));
        } else {
            params.setMargins(MySugarApp.getPixelsFromDPs(8), MySugarApp.getPixelsFromDPs(8), MySugarApp.getPixelsFromDPs(8), MySugarApp.getPixelsFromDPs(0));
        }
        viewHolder.thisCardView.setLayoutParams(params);

        Plan currentPlan = mDataSet.get(position);
        viewHolder.typeTextView.setText(currentPlan.getTypeName());
        viewHolder.moneyTextView.setText(
                MySugarApp.getApplicationResources().getString(R.string.stat_card_money_text_template,
                        currentPlan.getPrice(), "р"));//todo Валюты?
        Calendar calendar = currentPlan.getEndDate();
            viewHolder.dateTextView.setText(
                    MySugarApp.getApplicationResources().getString(R.string.date_text_template,
                            calendar.get(Calendar.DAY_OF_MONTH),
                            calendar.get(Calendar.MONTH) + 1,
                            calendar.get(Calendar.YEAR)));
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataSet.size();
    }
}