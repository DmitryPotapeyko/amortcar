package com.project.dmitry.amortcar.mainActivityPac;

import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.project.dmitry.amortcar.models.Plan;
import com.project.dmitry.amortcar.R;
import java.util.List;


public class StatRecyclerViewFragment extends Fragment {
    private final String TAG = "StatRecyclerViewFragment";
    private List<Plan> dataSet;
    private RecyclerView mRecyclerView;
    protected StatCustomAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_stat_recycler_view, container, false);
        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.stat_recycler_view);
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);
        if (dataSet != null) {
            mAdapter = new StatCustomAdapter(dataSet);
            mRecyclerView.setAdapter(mAdapter);
            if(savedInstanceState!=null&&savedInstanceState.getParcelable(TAG)!=null){
                mLayoutManager.onRestoreInstanceState(savedInstanceState.getParcelable(TAG));
            }
        }
        return rootView;
    }
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Parcelable state = mLayoutManager.onSaveInstanceState();
        outState.putParcelable(TAG,state);
    }
    public void setDataSet(List<Plan> dataSet){
        this.dataSet=dataSet;
    }
}
