package com.project.dmitry.amortcar.mainActivityPac;


import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import com.project.dmitry.amortcar.R;


public class MyAlertDialogs {
    public boolean isChecked() {
        return isChecked;
    }

    private boolean isChecked = false;

    public AlertDialog getSimpleAlertDialog(Context ctx, String title, String message,
                                            String positiveName,
                                            DialogInterface.OnClickListener positive,
                                            String negativeName,
                                            final DialogInterface.OnClickListener negative,
                                            boolean showRepeatCheckbox) {
        AlertDialog.Builder ad = new AlertDialog.Builder(ctx);
        ad.setTitle(title);
        ad.setMessage(message);
        ad.setPositiveButton(positiveName, positive);
        ad.setNegativeButton(negativeName, negative);
        ad.setCancelable(true);
        if(showRepeatCheckbox) {
            View checkBoxView = View.inflate(ctx, R.layout.check_box_view, null);
            CheckBox checkBox = checkBoxView.findViewById(R.id.checkbox);
            checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    MyAlertDialogs.this.isChecked = isChecked;
                }
            });
            checkBox.setText(R.string.repeat_plan_text);
            ad.setView(checkBoxView);
        }
        return ad.create();
    }
}



