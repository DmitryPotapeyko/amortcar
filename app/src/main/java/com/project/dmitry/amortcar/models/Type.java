package com.project.dmitry.amortcar.models;

import com.orm.SugarRecord;
import com.orm.dsl.Unique;

public final class Type extends SugarRecord {
    @Unique
    private long id;
    public String getName() {
        return name;
    }

    private String name;

    public Type() {
    }

    public Type(String name) {
        this.name = name;
    }
}
