package com.project.dmitry.amortcar.models;


import com.orm.SugarRecord;
import com.orm.dsl.Ignore;
import com.orm.dsl.Unique;
import com.project.dmitry.amortcar.MySugarApp;

import java.util.Calendar;

public class Plan extends SugarRecord  {
    /* необходимые значения;
    тип

    даты:
        дата начала отсчета
        период (через сколько времени) (ms)
        дата конца отсчета
        прошедшее время (ms)

    Расстояния:
        пробег начала отсчета
        период (через сколько километров)
        уже пройденный километраж

    Деньги:
        цена
        текущая амотризация (рассчитывается исходя из даты начала и текущей даты/ пробега начала и текущего пробега)
        текущая стоимость километра (для планов по километражу)
     */
    @Unique
    private long id;

    public Car getCar() {
        return car;
    }

    public Type getType() {
        return type;
    }

    public Milage getStartMilage() {
        return startMilage;
    }

    @SuppressWarnings("FieldCanBeLocal")
    private Car car;
    private Type type;
    private double price;
    private Calendar startDate;
    private Milage startMilage;
    private Integer lengthPeriod;
    private Calendar endDate;
    @SuppressWarnings("FieldCanBeLocal")
    private int percentOfPassed;//used for sort

    public boolean isCloseStatus() {
        return closeStatus;
    }

    private boolean closeStatus;
    /*
        It is necessary to call updatePassedParametrs in getters
        that return calculated parameters (not stored in the database)
    */


    @Ignore
    private Long timePeriod = null;
    @Ignore
    private Long passedTime = null;
    @Ignore
    private double todayAmort;
    @Ignore
    private Integer passedLength = null;
    @Ignore
    private boolean isUpDate = false;

    public Plan() {
    }


    public double getCurrentCostOfKm() {
        if (this.lengthPeriod != null && getPassedLen() != null && lengthPeriod > getPassedLen()) {
            if (lengthPeriod.compareTo(0) != 0) {
                return price / lengthPeriod;
            }
        }
        return 0;
    }

    public String getTypeName() {
        return this.type.getName();
    }

    public Calendar getStartDate() {
        return startDate;
    }

    public Long getTimePeriod() {
        if (!isUpDate) {
            updatePassedParametrs();
        }
        return timePeriod;
    }

    public Calendar getEndDate() {
        return endDate;
    }

    public Long getPassedTime() {
        if (!isUpDate) {
            updatePassedParametrs();
        }
        return passedTime;
    }

    public Integer getStartLength() {
        if (startMilage != null) {
            return startMilage.getLength();
        }
        return null;
    }

    public Integer getLengthPeriod() {
        return lengthPeriod;
    }

    private Integer getPassedLen() {
        if (!isUpDate) {
            updatePassedParametrs();
        }
        return passedLength;
    }

    public Integer getPassedLength() {
        return getPassedLen();
    }

    public double getPrice() {
        return price;
    }

    public double getTodayAmort() {
        if (!isUpDate) {
            updatePassedParametrs();
        }
        return todayAmort;
    }


    public double getMoneyWithoutUpdateIn(int milage, Calendar date){
        passedTime = date.getTimeInMillis() - startDate.getTimeInMillis();
        if (startMilage != null) {
            passedLength = milage - startMilage.getLength();
        }
        double todayAmortLen = -1;
        if (passedLength != null && lengthPeriod != null) {
            int len = passedLength;
            if (passedLength > lengthPeriod) {
                len = lengthPeriod;
            }
            todayAmortLen = price * len / lengthPeriod;
        }

        double todayAmortTime = -1;

        if (timePeriod != null && passedTime != null) {
            long t = passedTime;
            if (passedTime > timePeriod) {
                t = timePeriod;
            }
            todayAmortTime = price * t / timePeriod;
        }
        todayAmort = todayAmortLen > todayAmortTime ?
                todayAmortLen : todayAmortTime;
        return todayAmort;
    }
    private void updatePassedParametrs() {
        updatePassed(MySugarApp.getLastMilageOfSelectedCar(), Calendar.getInstance());
    }

    public void updatePassedParametrs(Milage nowMilage, Calendar date) {
        updatePassed(nowMilage, date);
    }

    private void updatePassed(Milage nowMilage, Calendar date) {
//        startDate can't be null
        passedTime = date.getTimeInMillis() - startDate.getTimeInMillis();
        if (endDate != null) {
            timePeriod = endDate.getTimeInMillis() - startDate.getTimeInMillis();
        }

        if (startMilage != null) {
            passedLength = nowMilage.getLength() - startMilage.getLength();
        }
        double todayAmortLen = -1;
        if (passedLength != null && lengthPeriod != null) {
            int len = passedLength;
            if (passedLength > lengthPeriod) {
                len = lengthPeriod;
            }
            todayAmortLen = price * len / lengthPeriod;
        }

        double todayAmortTime = -1;

        if (timePeriod != null && passedTime != null) {
            long t = passedTime;
            if (passedTime > timePeriod) {
                t = timePeriod;
            }
            todayAmortTime = price * t / timePeriod;
        }
        todayAmort = todayAmortLen > todayAmortTime ?
                todayAmortLen : todayAmortTime;
        percentOfPassed = (int) (todayAmort * 100 / price);
        this.update();
        isUpDate = true;
    }
    public void closePlan(){
        closeStatus=true;//????
        endDate = Calendar.getInstance();
    }
    public void closePlanInEndDate(){
        closeStatus=true;//????
    }


    public Plan(Car car, Type type, double price, Calendar startDate, Calendar endDate, Milage startMilage, Integer lengthPeriod) {
        this.car = car;
        this.type = type;
        this.price = price;
        this.startDate = startDate;
        this.endDate = endDate;
        this.startMilage = startMilage;
        this.lengthPeriod = lengthPeriod;
        this.closeStatus = false;
    }

    public double getDopMoneyWithoutUpdateIn(int dopMilage, Calendar instance) {

        Milage m = MySugarApp.getLastMilageOfSelectedCar();
        long passedTime = m.getDate().getTimeInMillis() - startDate.getTimeInMillis();
        long dopPassedTime = instance.getTimeInMillis() - m.getDate().getTimeInMillis();
        Integer passedLength = null;

        if (startMilage != null) {
            passedLength = m.getLength() - startMilage.getLength();
        }
        double dopAmortLen = 0;
        if (passedLength != null && lengthPeriod != null) {
            int len = passedLength;
            if (passedLength >= lengthPeriod) {
                len = 0;
            }else if(passedLength+dopMilage>lengthPeriod){
                len = lengthPeriod-passedLength;
            }else{
                len = dopMilage;
            }
            dopAmortLen = price * len / lengthPeriod;
        }
        return dopAmortLen;
    }
}
