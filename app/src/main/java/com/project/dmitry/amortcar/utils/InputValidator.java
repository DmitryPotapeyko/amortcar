package com.project.dmitry.amortcar.utils;


public class InputValidator {
    public static Integer integerDecode(String number) throws InputFormatException {
        if (number==null){throw new InputFormatException("number param == null");}
        if (number.startsWith("0")&&number.length()>1){throw new InputFormatException("number param Start with 0");}
        if (number.equals("")){throw new InputFormatException("number param is empty");}
        try {
            return Integer.decode(number);
        }catch (NumberFormatException ex){
            throw new InputFormatException("Can't decode number param");
        }
    }
    public static Double doubleDecode(String number) throws InputFormatException {
        if (number==null){throw new InputFormatException("number param == null");}
        if (number.equals("")){throw new InputFormatException("number param is empty");}
        try {
            return Double.valueOf(number);
        }catch (NumberFormatException ex){
            throw new InputFormatException("Can't decode number param");
        }
    }
}
