package com.project.dmitry.amortcar.mainActivityPac;

import android.content.DialogInterface;
import android.content.res.Resources;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.project.dmitry.amortcar.models.Plan;
import com.project.dmitry.amortcar.MySugarApp;
import com.project.dmitry.amortcar.planActivityPac.PlanActivity;
import com.project.dmitry.amortcar.R;
import com.project.dmitry.amortcar.utils.MLog;

import java.util.Calendar;
import java.util.List;

/**
 * Provide views to RecyclerView with data from mDataSet.
 */
public class CustomAdapter extends RecyclerView.Adapter<CustomAdapter.ViewHolder> {
    public interface OnItemClickListener {
        void onItemClick(View itemView, int position, View view);
    }

    public void OnItemClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }

    OnItemClickListener listener;

    private static final String TAG = "CustomAdapter";
    private List<Plan> mDataSet;
    private MainActivity activity;

    /**
     * Provide a reference to the type of views that you are using (custom ViewHolder)
     */
    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnCreateContextMenuListener {
        private final TextView typeTextView, dateTextView, milageTextView, moneyTextView;
        private final ProgressBar dateProgressBar, milageProgressBar, moneyProgressBar;
        private final LinearLayout dateLayout, milageLayout;
        private final ImageButton button;
        private final CardView thisCardView;


        ViewHolder(CardView v) {
            super(v);
            // Define click listener for the ViewHolder's View.
            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    MLog.log(TAG, "Element " + getAdapterPosition() + " clicked.");
                }
            });
            thisCardView = v;
            typeTextView = v.findViewById(R.id.plan_card_type);
            dateTextView = v.findViewById(R.id.plan_card_date);
            milageTextView = v.findViewById(R.id.plan_card_milage);
            moneyTextView = v.findViewById(R.id.plan_card_money);
            dateProgressBar = v.findViewById(R.id.plan_card_date_progress_bar);
            milageProgressBar = v.findViewById(R.id.plan_card_milage_progress_bar);
            moneyProgressBar = v.findViewById(R.id.plan_card_money_progress_bar);
            milageLayout = v.findViewById(R.id.plan_card_progress_milage_layout);
            dateLayout = v.findViewById(R.id.plan_card_date_layout);
            button = v.findViewById(R.id.plan_card_button);
            button.setOnClickListener(this);
            //button.setOnCreateContextMenuListener(this);
        }

        @Override
        public void onClick(View v) {
            showPopupMenu(itemView, v, getLayoutPosition());
        }

        private void showPopupMenu(final View itemView, final View v, final int layoutPosition) {
            PopupMenu popupMenu = new PopupMenu(itemView.getContext(), v);
            popupMenu.inflate(R.menu.popap_in_plans);
            popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

                @Override
                public boolean onMenuItemClick(MenuItem item) {
                    // Toast.makeText(PopupMenuDemoActivity.this,
                    // item.toString(), Toast.LENGTH_LONG).show();
                    // return true;
                    final MyAlertDialogs maker = new MyAlertDialogs();
                    Resources r;
                    switch (item.getItemId()) {
                        case R.id.menu1:
                            r =  MySugarApp.getApplicationResources();
                            final AlertDialog alertDialig1 = maker.getSimpleAlertDialog(itemView.getContext(),
                                    r.getString(R.string.completion_dialog_title),
                                    r.getString(R.string.сompletion_dialog_text),
                                    r.getString(R.string.completion_dialog_ok),
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {

                                            Plan p = mDataSet.get(layoutPosition);
                                            PlanInfoTransformer transformer = new PlanInfoTransformer(
                                                    p.getCar(),p.getType(),p.getPrice(),p.getStartDate(),p.getEndDate(),
                                                    p.getLengthPeriod());
                                            p.closePlan();
                                            p.update();
                                            mDataSet.remove(layoutPosition);
                                            notifyItemRemoved(layoutPosition);
                                            notifyItemRangeChanged(layoutPosition, mDataSet.size());
                                            notifyDataSetChanged();
                                            activity.startPlansAsyncTask();
                                            if(maker.isChecked()){
                                                PlanActivity.start(activity,transformer);
                                            }
                                        }
                                    },
                                    r.getString(R.string.dialog_cancel),
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {

                                        }
                                    },true
                                    );
                            r=null;
                            alertDialig1.show();
                            return true;


//                        case R.id.menu2:

//                            return true;
                        case R.id.menu3: {
                            r=  MySugarApp.getApplicationResources();
                            final AlertDialog alertDialig3 = maker.getSimpleAlertDialog(itemView.getContext(),
                                    r.getString(R.string.delete_dialog_title),
                                    r.getString(R.string.delete_dialog_text),
                                    r.getString(R.string.delete_dialog_ok),
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            Plan p = mDataSet.get(layoutPosition);
                                            p.delete();
                                            mDataSet.remove(layoutPosition);
                                            notifyItemRemoved(layoutPosition);
                                            notifyItemRangeChanged(layoutPosition, mDataSet.size());
                                            notifyDataSetChanged();
                                            activity.startPlansAsyncTask();
                                        }
                                    },
                                    r.getString(R.string.dialog_cancel),
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {

                                        }
                                    },false);
                            r=null;
                            alertDialig3.show();
                            return true;
                        }
                    }
                    return false;
                }
            });
            popupMenu.show();
        }

        @Override
        public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
            menu.setHeaderTitle("Select The Action");
            menu.add(0, v.getId(), 0, "Call");//groupId, itemId, order, title
            menu.add(0, v.getId(), 0, "SMS");
        }
    }

    /**
     * Initialize the dataset of the Adapter.
     *
     * @param dataSet List<Plan> containing the data to populate views to be used by RecyclerView.
     */
    CustomAdapter(List<Plan> dataSet, @NonNull MainActivity activity) {
        mDataSet = dataSet;
        this.activity = activity;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        // Create a new view.
        CardView v = (CardView) LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.plan_card_view, viewGroup, false);
        return new ViewHolder(v);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {
        MLog.log(TAG, "Element " + position + " set.");

        // Get element from your dataset at this position and replace the contents of the view
        // with that element
        ViewGroup.MarginLayoutParams params =
                (ViewGroup.MarginLayoutParams) viewHolder.thisCardView.getLayoutParams();
        if (position + 1 == getItemCount()) {
            params.setMargins(MySugarApp.getPixelsFromDPs(8), MySugarApp.getPixelsFromDPs(8), MySugarApp.getPixelsFromDPs(8), MySugarApp.getPixelsFromDPs(80));
        } else {
            params.setMargins(MySugarApp.getPixelsFromDPs(8), MySugarApp.getPixelsFromDPs(8), MySugarApp.getPixelsFromDPs(8), MySugarApp.getPixelsFromDPs(0));
        }
        viewHolder.thisCardView.setLayoutParams(params);

        Plan currentPlan = mDataSet.get(position);
        viewHolder.typeTextView.setText(currentPlan.getTypeName());
        viewHolder.moneyTextView.setText(
                MySugarApp.getApplicationResources().getString(R.string.plan_card_money_text_template,
                        currentPlan.getTodayAmort(),
                        currentPlan.getPrice(),MySugarApp.getApplicationResources().getString(R.string.ruble )));//todo Валюты?

        int passedMoneyPercent = (int) (currentPlan.getTodayAmort() * 100 / currentPlan.getPrice());
        viewHolder.moneyProgressBar.setProgress(passedMoneyPercent);


        if (currentPlan.getTimePeriod() != null) {
            viewHolder.dateLayout.setVisibility(View.VISIBLE);
            Calendar calendar = currentPlan.getEndDate();
            viewHolder.dateTextView.setText(
                    MySugarApp.getApplicationResources().getString(R.string.date_text_template,
                            calendar.get(Calendar.DAY_OF_MONTH),
                            calendar.get(Calendar.MONTH) + 1,
                            calendar.get(Calendar.YEAR)));

            int readinessTimePercent = (int) (currentPlan.getPassedTime() * 100 / currentPlan.getTimePeriod());
            viewHolder.dateProgressBar.setProgress(readinessTimePercent);
        } else {
            viewHolder.dateLayout.setVisibility(View.GONE);
        }


        if (currentPlan.getStartLength() != null && currentPlan.getPassedLength() != null) {
            viewHolder.milageLayout.setVisibility(View.VISIBLE);
            viewHolder.milageTextView.setText(
                    MySugarApp.getApplicationResources().getString(R.string.plan_card_length_text_template,
                            currentPlan.getPassedLength(),
                            currentPlan.getLengthPeriod()));

            int readinessLengthPercent = currentPlan.getPassedLength() * 100 / currentPlan.getLengthPeriod();
            viewHolder.milageProgressBar.setProgress(readinessLengthPercent);
        } else {
            viewHolder.milageLayout.setVisibility(View.GONE);
        }
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataSet.size();
    }
}