package com.project.dmitry.amortcar.utils;

import android.util.Log;

import com.project.dmitry.amortcar.BuildConfig;

public class MLog {
    public static void log(String TAG,String msg){
        if(BuildConfig.DEBUG){
            Log.d(TAG,msg);
        }
    }
}
