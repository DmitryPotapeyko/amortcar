package com.project.dmitry.amortcar.statisticsActivityPac;

import android.annotation.SuppressLint;
import android.content.Context;
import android.widget.TextView;

import com.github.mikephil.charting.components.MarkerView;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.utils.MPPointF;
import com.github.mikephil.charting.utils.Utils;
import com.project.dmitry.amortcar.R;

/**
 * Custom implementation of the MarkerView.
 */
@SuppressLint("ViewConstructor")
public class KeyMarkerView extends MarkerView {

    private TextView tvContent;
    private TextView tvType;

    public KeyMarkerView(Context context, int layoutResource) {
        super(context, layoutResource);

        tvContent = (TextView) findViewById(R.id.tvContent);
        tvType = (TextView) findViewById(R.id.tvType);
    }

    // callbacks everytime the MarkerView is redrawn, can be used to update the
    // content (user-interface)
    @Override
    public void refreshContent(Entry e, Highlight highlight) {

            tvContent.setText(Utils.formatNumber(e.getY(), 0, true)
                    + getResources().getString(R.string.ruble));
            tvType.setText(e.getData().toString());

        super.refreshContent(e, highlight);
    }

    @Override
    public MPPointF getOffset() {
        return new MPPointF(-(getWidth() / 2), -getHeight());
    }
}
