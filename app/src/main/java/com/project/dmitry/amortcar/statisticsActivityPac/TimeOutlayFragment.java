package com.project.dmitry.amortcar.statisticsActivityPac;


import android.graphics.Color;
import android.graphics.Point;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.formatter.LargeValueFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.project.dmitry.amortcar.mainActivityPac.StatCustomAdapter;
import com.project.dmitry.amortcar.models.Milage;
import com.project.dmitry.amortcar.models.Plan;
import com.project.dmitry.amortcar.MySugarApp;
import com.project.dmitry.amortcar.R;
import com.project.dmitry.amortcar.utils.MLog;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TimeOutlayFragment extends Fragment implements OnChartValueSelectedListener, MyDataListeners {

    BarChart barChart;
    private KeyMarkerView mv;
    Button moreTextView;

    RecyclerView infoRecyclerView;

    TextView recyclerScroller;
    LinearLayout progressView;
    LinearLayout contentView;

    private String selectedType;
    HashMap<String, ArrayList<Plan>> typesPlans;

    protected StatCustomAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    public boolean isViewCreated = false;
    private MoreTextViewListener moreTextViewListener;

    public TimeOutlayFragment() {
        // Required empty public constructor
    }

    public static TimeOutlayFragment newInstance() {
        return new TimeOutlayFragment();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_time_outlay, container, false);
        barChart = view.findViewById(R.id.fr_time_outlay_barChar);
        moreTextView = view.findViewById(R.id.fr_time_outlay_more);
        infoRecyclerView = view.findViewById(R.id.fr_time_outlay_more_recycler);
        recyclerScroller = view.findViewById(R.id.fr_time_outlay_more_scroller);
        progressView = view.findViewById(R.id.fr_time_outlay_progress_view);
        contentView = view.findViewById(R.id.fr_time_outlay_content);

        barChart.setOnChartValueSelectedListener(this);
        barChart.getDescription().setEnabled(false);
        barChart.setDrawBarShadow(false);
        barChart.setDrawGridBackground(false);
        // create a custom MarkerView (extend MarkerView) and specify the layout
        // to use for it
        mv = new KeyMarkerView(getContext(), R.layout.custom_marker_view);
        mv.setChartView(barChart); // For bounds control
        barChart.setMarker(mv); // Set the marker to the chart

        Legend l = barChart.getLegend();
        l.setEnabled(false);
//        l.setVerticalAlignment(Legend.LegendVerticalAlignment.BOTTOM);
//        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.LEFT);
//        l.setOrientation(Legend.LegendOrientation.HORIZONTAL);
//        l.setDrawInside(false);
//        l.setYOffset(0f);
//        l.setXOffset(10f);
//        l.setYEntrySpace(0f);
//        l.setTextSize(8f);
//        l.setWordWrapEnabled(true);

        XAxis xAxis = barChart.getXAxis();
        xAxis.setDrawGridLines(false);
        xAxis.setEnabled(false);

        YAxis leftAxis = barChart.getAxisLeft();
        leftAxis.setValueFormatter(new LargeValueFormatter());
        leftAxis.setSpaceTop(35f);
        leftAxis.setAxisMinimum(0f); // this replaces setStartAtZero(true)
        barChart.getAxisRight().setEnabled(false);

        infoRecyclerView.setVisibility(View.GONE);

        moreTextView.setVisibility(View.INVISIBLE);
        moreTextViewListener = new MoreTextViewListener();
        moreTextView.setOnClickListener(moreTextViewListener);
        progressView.setVisibility(View.VISIBLE);
        ((StatisticActivity) getActivity()).addDataListener(this);
        return view;
    }

    void updateOutlayBarChar(@NonNull List<Plan> mData) {
        HashMap<String, Double> typesMoney = new HashMap<>();
        typesPlans = new HashMap<>(mData.size());
        String name;
        Plan currentPlan;
        Double curMoney;
        for (Plan aMData : mData) {
            currentPlan = aMData;
            name = currentPlan.getTypeName();
            curMoney = typesMoney.put(name, currentPlan.getPrice());
            if (typesPlans.containsKey(name)) {
                typesPlans.get(name).add(aMData);
            } else {
                ArrayList<Plan> tPlans = new ArrayList<>();
                tPlans.add(aMData);
                typesPlans.put(name, tPlans);
            }
            if (curMoney != null) {
                typesMoney.put(name, curMoney + currentPlan.getPrice());
            }
        }

        int i = 0;
        String[] barColors = getResources().getStringArray(R.array.bar_colors);
        ArrayList<BarEntry> entry;
        BarDataSet set;
        BarData data = new BarData();
        for (Map.Entry<String, Double> item : typesMoney.entrySet()) {
            entry = new ArrayList<>(1);
            entry.add(new BarEntry(i++, (float) item.getValue().doubleValue(), item.getKey()));
            set = new BarDataSet(entry, item.getKey());
            set.setColor(Color.parseColor(barColors[i % barColors.length]));
            data.addDataSet(set);
        }
        data.setValueFormatter(new LargeValueFormatter());
        if(mData.size()>0) {
            barChart.setData(data);
        }else{
            barChart.setData(null);
            barChart.setNoDataText(getString(R.string.not_enough_data));
            barChart.setNoDataTextColor(Color.BLACK);
        }
        barChart.setDrawMarkers(false);//фикс баг при перезагрузке данных, небыло сброса маркера, и если он указывал на отсутствующий столбик. то nullPointer
        barChart.invalidate(); // refresh
        barChart.animateXY(750, 750);
    }


    @Override
    public void onDestroyView() {
        isViewCreated = false;
        ((StatisticActivity) getActivity()).removeDataListener(this);
        super.onDestroyView();
    }

    @Override
    public void onValueSelected(Entry e, Highlight h) {
        barChart.setDrawMarkers(true);//фикс а эта штука на первую отрисовку убирает маркер, поэтому падения нет
        moreTextViewListener.setStatus(0);
        moreTextView.setVisibility(View.VISIBLE);
        moreTextView.setText(getResources().getString(R.string.more_text));
        infoRecyclerView.setVisibility(View.GONE);
        BarEntry barEntry = (BarEntry) e;
        selectedType = barEntry.getData().toString();//it is a type;
    }

    @Override
    public void onNothingSelected() {
        moreTextView.setVisibility(View.INVISIBLE);
        moreTextViewListener.setStatus(0);
        infoRecyclerView.setVisibility(View.GONE);
    }

    @Override
    public void onPlansDataUpdate(List<Plan> data) {
        MLog.log("THREADSS", "onPlansDataUpdate " + Thread.currentThread().toString());
        progressView.setVisibility(View.GONE);
        contentView.setVisibility(View.VISIBLE);
        if (data != null) {
            updateOutlayBarChar(data);
        }
    }

    @Override
    public void onMilageDataUpdate(List<Milage> data) {

    }

    @Override
    public void onRefreshingStart() {
        MLog.log("THREADSS", "onRefreshingStart " + Thread.currentThread().toString());
        if (progressView != null)
            progressView.setVisibility(View.VISIBLE);
        contentView.setVisibility(View.INVISIBLE);
        moreTextView.setVisibility(View.INVISIBLE);
        infoRecyclerView.setVisibility(View.GONE);
    }

    @Override
    public void onPlanError() {
        progressView.setVisibility(View.GONE);
        contentView.setVisibility(View.VISIBLE);
        Toast.makeText(getContext(), "Произошла ошибка при загрузке данных", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onMilageError() {

    }


    private class MoreTextViewListener implements View.OnClickListener {
        void setStatus(int status) {
            this.status = status;
        }

        private int status = 0;

        @Override
        public void onClick(View v) {
            if (status == 0) {
                mLayoutManager = new LinearLayoutManager(getActivity());
                Display display = getActivity().getWindowManager().getDefaultDisplay();
                Point size = new Point();
                display.getSize(size);
                infoRecyclerView.setLayoutManager(mLayoutManager);
                if (typesPlans != null && selectedType != null) {
                    status = 1;
                    ArrayList<Plan> morePlans = typesPlans.get(selectedType);
                    ViewGroup.LayoutParams params = infoRecyclerView.getLayoutParams();
                    if (morePlans != null) {
                        if (morePlans.size() < 3) {
                            params.height = ViewGroup.LayoutParams.WRAP_CONTENT;
                            infoRecyclerView.setLayoutParams(params);
                        } else {
                            params.height = MySugarApp.getPixelsFromDPs(350);
                            infoRecyclerView.setLayoutParams(params);
                        }
                    }

                    mAdapter = new StatCustomAdapter(morePlans);
                    infoRecyclerView.setAdapter(mAdapter);
                    infoRecyclerView.setVisibility(View.VISIBLE);
                    ((TextView) v).setText(R.string.less_text);
                }
            } else if (status == 1) {
                ((TextView) v).setText(R.string.more_text);
                infoRecyclerView.setVisibility(View.GONE);
                status = 0;
            }
        }
    }
}

