package com.project.dmitry.amortcar.mainActivityPac;

import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.project.dmitry.amortcar.R;
import com.project.dmitry.amortcar.models.Plan;

import java.util.List;

public class PlansRecyclerViewFragment extends Fragment {
    private final String TAG = "PlansRecyclerViewFragment";
    private List<Plan> dataSet;
    private RecyclerView mRecyclerView;
    protected CustomAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_plan_recycler_view, container, false);
        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.plan_recycler_view);
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);
        if (dataSet != null) {
            mAdapter = new CustomAdapter(dataSet,(MainActivity)getActivity());
            mRecyclerView.setAdapter(mAdapter);
            if(savedInstanceState!=null&&savedInstanceState.getParcelable(TAG)!=null){
                mLayoutManager.onRestoreInstanceState(savedInstanceState.getParcelable(TAG));
            }
        }
        return rootView;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Parcelable state = mLayoutManager.onSaveInstanceState();
        outState.putParcelable(TAG,state);
    }

    public void setDataSet(List<Plan> dataSet){
        this.dataSet=dataSet;
    }
}
