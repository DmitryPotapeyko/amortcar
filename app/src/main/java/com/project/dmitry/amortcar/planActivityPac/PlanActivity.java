package com.project.dmitry.amortcar.planActivityPac;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.project.dmitry.amortcar.MySugarApp;
import com.project.dmitry.amortcar.R;
import com.project.dmitry.amortcar.mainActivityPac.MainActivity;
import com.project.dmitry.amortcar.mainActivityPac.PlanInfoTransformer;
import com.project.dmitry.amortcar.models.Car;
import com.project.dmitry.amortcar.models.Plan;
import com.project.dmitry.amortcar.models.Type;
import com.project.dmitry.amortcar.utils.DatePickerFragment;
import com.project.dmitry.amortcar.utils.DatePickerFragmentListener;
import com.project.dmitry.amortcar.utils.InputFormatException;
import com.project.dmitry.amortcar.utils.InputValidator;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;

public class PlanActivity extends AppCompatActivity implements DatePickerFragmentListener {
    private static String CAR_ID_EXTRA = "CAR_ID_EXTRA";
    private static String START_PLAN = "START_PLAN";


    private enum SelectedType {
        time, milage, time_and_milage
    }

    private SelectedType selectedType;
    @BindView(R.id.ac_plan_type_autocomplete)
    AutoCompleteTextView typeAutocomplete;
    @BindView(R.id.ac_plan_type_input_layout)
    TextInputLayout planTypeInputLayout;


    @BindView(R.id.ac_plan_periodType)
    Spinner periodType;

    @BindViews({R.id.ac_plan_year_input, R.id.ac_plan_month_input, R.id.ac_plan_price_input, R.id.ac_plan_start_time_input})
    List<TextView> timePerInputsViews;
    @BindViews({R.id.ac_plan_year_label, R.id.ac_plan_month_label, R.id.ac_plan_price_label, R.id.ac_plan_start_time_label})
    List<TextView> timePerlabelsViews;

    @BindView(R.id.ac_plan_milage_label)
    TextView milageLabelView;

    @BindView(R.id.ac_plan_milage_input)
    TextView milageInputView;

    @BindView(R.id.ac_plan_milage_layout)
    LinearLayout milageInputLayout;

    @BindView(R.id.ac_plan_time_period_layout)
    ConstraintLayout timePerInputLayout;


    @BindView(R.id.ac_plan_saveBtn)
    Button saveBtn;


    Calendar startDate;
    Car car;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_plan);
        ButterKnife.bind(this);

        timePerInputLayout.setVisibility(View.GONE);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setHomeButtonEnabled(true);
        }

        car = MySugarApp.getSelectedCar();
        if (car == null) {
            throw new IllegalStateException("PlanAc no car");
        }

//      types--------------------------------------
        ArrayAdapter<String> adapter = new ArrayAdapter<>(
                this, android.R.layout.simple_dropdown_item_1line);
        Iterator<Type> typeIterator = Type.findAll(Type.class);
        while (typeIterator.hasNext()) {
            adapter.add(typeIterator.next().getName());
        }
        typeAutocomplete.setAdapter(adapter);
        typeAutocomplete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View arg0) {
                typeAutocomplete.showDropDown();
            }
        });
//      types--------------------------------------
//      Date---------------------------------------
        Calendar c = Calendar.getInstance();
        onDateSet(c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH), null);
        timePerInputsViews.get(3).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DatePickerFragment newFragment = new DatePickerFragment();
                newFragment.setDatePickerListener(PlanActivity.this);
                newFragment.show(getSupportFragmentManager(), "datePicker");
            }
        });
//      Date---------------------------------------
//      Save---------------------------------------
        saveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Plan p = checkInputs();
                if (p != null) {
                    p.save();
                    MainActivity.start(PlanActivity.this);
                }
            }
        });
//      Save---------------------------------------
//      PeriodType---------------------------------
        periodType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {
                    case 0:
                        timePerInputLayout.setVisibility(View.GONE);
                        milageInputLayout.setVisibility(View.VISIBLE);
                        selectedType = SelectedType.milage;
                        break;

                    case 1:
                        timePerInputLayout.setVisibility(View.VISIBLE);
                        milageInputLayout.setVisibility(View.VISIBLE);
                        selectedType = SelectedType.time_and_milage;
                        break;
                    case 2:
                    default: {
                        timePerInputLayout.setVisibility(View.VISIBLE);
                        milageInputLayout.setVisibility(View.GONE);
                        selectedType = SelectedType.time;
                        break;
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        Serializable startPlan = getIntent().getSerializableExtra(START_PLAN);
        if (startPlan != null) {
            PlanInfoTransformer p = (PlanInfoTransformer) startPlan;
            if (MySugarApp.getSelectedCar() == null || !Objects.equals(p.getCarId(), MySugarApp.getSelectedCar().getId()))
                return;
            //periodType
            int perType = 0;
            if (p.getLengthPeriod() != null) {
                perType = 0;
                milageInputView.setText(String.valueOf(p.getLengthPeriod()));
                if (p.getStartDate() != null && p.getEndDate() != null) {
                    perType = 1;
                    long days = TimeUnit.MILLISECONDS.toDays(
                            p.getEndDate().getTimeInMillis() - p.getStartDate().getTimeInMillis());
                    int month = (int) (((double) days / 30) + 0.5);
                    timePerInputsViews.get(1).setText(String.valueOf(month));
                }
            } else {
                if (p.getStartDate() != null && p.getEndDate() != null) {
                    perType = 2;
                    long days = TimeUnit.MILLISECONDS.toDays(
                            p.getEndDate().getTimeInMillis() - p.getStartDate().getTimeInMillis());
                    int month = (int) (((double) days / 30) + 0.5);
                    timePerInputsViews.get(1).setText(String.valueOf(month));
                } else {
                    throw new RuntimeException("No periodType in plan");
                }
            }
            periodType.setSelection(perType);
            typeAutocomplete.setText(p.getTypeName());
            timePerInputsViews.get(2).setText(String.valueOf(p.getPrice()));


        }
    }

    private Plan checkInputs() {
        String yearsPeriod;
        int parsedYearsPeriod;
        String monthPeriod;
        int parsedMonthPeriod;
        String price;
        int parsedMilagePeriod = 0;
        double parsedPrice = 0;
        String type;
        boolean isErrorInput = false;
        Type nowType = null;
        Calendar endDate = null;

        price = timePerInputsViews.get(2).getText().toString();
        type = typeAutocomplete.getText().toString();

        //delete errorsMessage
        planTypeInputLayout.setErrorEnabled(false);
        for (TextView t : timePerlabelsViews) {
            t.setTextColor(getResources().getColor(android.R.color.black));
        }
        milageLabelView.setTextColor(getResources().getColor(android.R.color.black));
        //get Common Fileld Values
        if (type.equals("")) {
            planTypeInputLayout.setError(getString(R.string.empty_required_field));
            isErrorInput = true;
        } else {
            List<Type> t = Type.find(Type.class, "name=?", new String[]{type}, null, null, "1");
            if (t.isEmpty()) {
                nowType = new Type(type);
                nowType.save();
            } else {
                nowType = t.get(0);
            }
        }

        try {
            parsedPrice = InputValidator.doubleDecode(price);
        } catch (InputFormatException e) {
            timePerlabelsViews.get(2).setTextColor(getResources().getColor(android.R.color.holo_red_light));
            isErrorInput = true;
        }

        if (selectedType == SelectedType.time || selectedType == SelectedType.time_and_milage) {
            yearsPeriod = timePerInputsViews.get(0).getText().toString();
            monthPeriod = timePerInputsViews.get(1).getText().toString();
            if (yearsPeriod.equals("") && monthPeriod.equals("")) {
                timePerlabelsViews.get(0).setTextColor(getResources().getColor(android.R.color.holo_red_light));
                timePerlabelsViews.get(1).setTextColor(getResources().getColor(android.R.color.holo_red_light));
                isErrorInput = true;
            } else {
                yearsPeriod = yearsPeriod.equals("") ? "0" : yearsPeriod;
                monthPeriod = monthPeriod.equals("") ? "0" : monthPeriod;
                endDate = (Calendar) startDate.clone();
                try {
                    parsedYearsPeriod = InputValidator.integerDecode(yearsPeriod);
                    endDate.add(Calendar.YEAR, parsedYearsPeriod);
                } catch (InputFormatException e) {
                    timePerInputsViews.get(0).setError(getString(R.string.format_error_text));
                    isErrorInput = true;
                }
                try {
                    parsedMonthPeriod = InputValidator.integerDecode(monthPeriod);
                    endDate.add(Calendar.MONTH, parsedMonthPeriod);
                } catch (InputFormatException e) {
                    timePerInputsViews.get(1).setError(getString(R.string.format_error_text));
                    isErrorInput = true;
                }
            }
        }

        if (selectedType == SelectedType.milage || selectedType == SelectedType.time_and_milage) {
            String milageInput = milageInputView.getText().toString();
            try {
                parsedMilagePeriod = InputValidator.integerDecode(milageInput);
            } catch (InputFormatException e) {
                milageLabelView.setTextColor(getResources().getColor(android.R.color.holo_red_light));
                isErrorInput = true;
            }
        }
        if (isErrorInput) {
            return null;
        }

        if (selectedType == SelectedType.milage) {
            return new Plan(car, nowType, parsedPrice, Calendar.getInstance(), null, MySugarApp.getLastMilageOfSelectedCar(), parsedMilagePeriod);
        }
        if (selectedType == SelectedType.time) {
            return new Plan(car, nowType, parsedPrice, startDate, endDate, null, null);
        }
        if (selectedType == SelectedType.time_and_milage) {
            return new Plan(car, nowType, parsedPrice, startDate, endDate, MySugarApp.getLastMilageOfSelectedCar(), parsedMilagePeriod);
        }
        return null;
    }


    public static void start(Context ctx) {
        Intent in = new Intent(ctx, PlanActivity.class);
        ctx.startActivity(in);
    }

    public static void start(Context ctx, PlanInfoTransformer notSavedPlan) {
        Intent in = new Intent(ctx, PlanActivity.class);
        in.putExtra(START_PLAN, notSavedPlan);
        ctx.startActivity(in);
    }

    @Override
    public void onDateSet(int year, int month, int day, String tag) {
        timePerInputsViews.get(3).setText(getResources().getString(R.string.date_text_template, day, month, year));
        Calendar c = Calendar.getInstance();
        c.set(year, month, day);
        startDate = c;
    }
}
