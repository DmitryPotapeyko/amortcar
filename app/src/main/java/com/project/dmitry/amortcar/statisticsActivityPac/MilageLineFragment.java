package com.project.dmitry.amortcar.statisticsActivityPac;


import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.project.dmitry.amortcar.models.Car;
import com.project.dmitry.amortcar.models.Milage;
import com.project.dmitry.amortcar.models.Plan;
import com.project.dmitry.amortcar.R;
import com.project.dmitry.amortcar.utils.MLog;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class MilageLineFragment extends Fragment implements OnChartValueSelectedListener, MyDataListeners {

    LineChart mChart;
    LinearLayout progressView;
    List<Milage> milages;
    ArrayList<Car> cars;
    private int [] colors;

    public MilageLineFragment() {
        // Required empty public constructor
    }

    public static MilageLineFragment newInstance() {
        return new MilageLineFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String [] col = getResources().getStringArray(R.array.bar_colors);
        colors = new int[col.length];
        int l = 0;
        for(String c:col){
            colors[l++]=Color.parseColor(c);
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_milage_line, container, false);
        progressView = view.findViewById(R.id.fr_milage_line_progress_view);

        mChart = view.findViewById(R.id.fr_milage_line_linechart);
        mChart.setOnChartValueSelectedListener(this);
        // enable touch gestures
        mChart.setTouchEnabled(true);
        // enable scaling and dragging
        mChart.setDragEnabled(true);
        mChart.setScaleEnabled(true);
        mChart.setDescription(null);
        // if disabled, scaling can be done on x- and y-axis separately
        XAxis xAxis = mChart.getXAxis();
        xAxis.setEnabled(false);

        XYDateMarkerView mv = new XYDateMarkerView(getContext(), R.layout.custom_marker_view);
        mv.setChartView(mChart); // For bounds control
        mChart.setMarker(mv); // Set the marker to the chart

        progressView.setVisibility(View.VISIBLE);
        ((StatisticActivity) getActivity()).addDataListener(this);
        return view;
    }

    void updateMilageLineChar() {
        if (milages == null || milages.size() == 0) {
            return;
        }
        ArrayList<ILineDataSet> dataSets = new ArrayList<>();
        cars = new ArrayList<>();
        boolean moreCars = false;
        ArrayList<ArrayList<Entry>> sets = new ArrayList<>(1);

        for (Milage m : milages) {
            if (!cars.contains(m.getCar())) {
                cars.add(m.getCar());
                sets.add(new ArrayList<Entry>());
            }
            ArrayList<Entry> values = sets.get(cars.indexOf(m.getCar()));
            Calendar date = m.getDate();
            float nowMin = TimeUnit.MILLISECONDS.toHours(date.getTimeInMillis());
            values.add(new Entry(nowMin, m.getLength(), date));
        }
        for (int i=0;i<sets.size();i++) {
            LineDataSet set1 = new LineDataSet(sets.get(i),cars.get(i).getTitle());
            set1.setDrawIcons(false);
            // set the line to be drawn like this "- - - - - -"
            set1.enableDashedHighlightLine(10f, 5f, 0f);
            set1.setColor(colors[i%colors.length]);
            set1.setCircleColor(colors[i%colors.length]);
            set1.setLineWidth(1f);
            set1.setCircleRadius(3f);
            set1.setDrawCircleHole(false);
            set1.setValueTextSize(9f);
            set1.setDrawFilled(false);
            set1.setFormLineWidth(1f);
            set1.setFormSize(15.f);

            set1.setMode(LineDataSet.Mode.LINEAR);
            set1.setDrawValues(false);
            dataSets.add(set1); // add the datasets
        }

        // create a data object with the datasets
        LineData data = new LineData(dataSets);
        // set data
        mChart.setData(data);
        mChart.setDrawMarkers(false);//фикс баг при перезагрузке данных, небыло сброса маркера, и если он указывал на отсутствующий столбик. то nullPointer
        mChart.invalidate(); // refresh
        mChart.animateXY(750,750);
    }

    @Override
    public void onDestroyView() {
        ((StatisticActivity) getActivity()).removeDataListener(this);
        super.onDestroyView();
    }


    @Override
    public void onPlansDataUpdate(List<Plan> data) {

    }

    @Override
    public void onMilageDataUpdate(final List<Milage> data) {
        MLog.log("THREADSS", "onPlansDataUpdate " + Thread.currentThread().toString());
        if (data != null) {
            milages = data;
            updateMilageLineChar();
            progressView.setVisibility(View.GONE);
            mChart.setVisibility(View.VISIBLE);
        } else {
            Thread th = new Thread(new Runnable() {
                @Override
                public void run() {
                    milages = Milage.listAll(Milage.class);
                }
            });
            th.start();
            try {
                th.join();
                updateMilageLineChar();
                progressView.setVisibility(View.GONE);
                mChart.setVisibility(View.VISIBLE);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onRefreshingStart() {
        if (progressView != null)
            progressView.setVisibility(View.VISIBLE);
        mChart.setVisibility(View.INVISIBLE);
    }

    @Override
    public void onPlanError() {

    }

    @Override
    public void onMilageError() {
        progressView.setVisibility(View.GONE);
        mChart.setVisibility(View.VISIBLE);
        Toast.makeText(getContext(), "Произошла ошибка при загрузке данных", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onValueSelected(Entry e, Highlight h) {
        mChart.setDrawMarkers(true);//фикс а эта штука на первую отрисовку убирает маркер, поэтому падения нет
    }

    @Override
    public void onNothingSelected() {

    }
}

