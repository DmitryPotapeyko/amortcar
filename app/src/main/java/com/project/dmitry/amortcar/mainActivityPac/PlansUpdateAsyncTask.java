package com.project.dmitry.amortcar.mainActivityPac;

import android.os.AsyncTask;

import com.project.dmitry.amortcar.models.Plan;

import java.lang.ref.WeakReference;
import java.util.List;

public class PlansUpdateAsyncTask extends AsyncTask<List<Plan>, Void, PlansUpdateAsyncTask.Result> {
    WeakReference<MainActivity> mainActivityWeakReference;
    public PlansUpdateAsyncTask(MainActivity mainActivity) {
        mainActivityWeakReference = new WeakReference<>(mainActivity);
    }

    public static class Result {
        double costPerKm;
        double money;

        public double getCostPerKm() {
            return costPerKm;
        }

        public double getMoney() {
            return money;
        }
    }



    @SafeVarargs
    @Override
    protected final Result doInBackground(List<Plan>... lists) {
        Result result = new Result();
        if (lists != null && lists.length > 0) {
            for (Plan p : lists[0]) {
                result.money += p.getTodayAmort();
                result.costPerKm += p.getCurrentCostOfKm();
            }
        }
        return result;
    }

    @Override
    protected void onPostExecute(Result result) {
        if(mainActivityWeakReference.get()!=null){
            mainActivityWeakReference.get().onPlansUpdateAsyncTaskResult(result);
        }
    }
}
