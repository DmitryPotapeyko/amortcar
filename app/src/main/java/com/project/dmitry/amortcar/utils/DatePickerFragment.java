package com.project.dmitry.amortcar.utils;


import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.widget.DatePicker;

import java.util.Calendar;

/**
 * Created by DmitryPC on 18.01.2018.
 */

@SuppressWarnings("DefaultFileTemplate")
public class DatePickerFragment extends DialogFragment
        implements DatePickerDialog.OnDateSetListener{
    private DatePickerFragmentListener listener;
    public void setDatePickerListener(DatePickerFragmentListener listener){
        this.listener=listener;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);

        // Create a new instance of DatePickerDialog and return it
        return new DatePickerDialog(getActivity(), this, year, month, day);
    }

    @Override
    public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
       if(listener!=null){
           datePicker.getTag();
           listener.onDateSet(i,i1,i2,getTag());
       }
    }
}
