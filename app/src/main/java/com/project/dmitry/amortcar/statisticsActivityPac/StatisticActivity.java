package com.project.dmitry.amortcar.statisticsActivityPac;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.project.dmitry.amortcar.models.Car;
import com.project.dmitry.amortcar.models.Milage;
import com.project.dmitry.amortcar.models.Plan;
import com.project.dmitry.amortcar.MySugarApp;
import com.project.dmitry.amortcar.R;
import com.project.dmitry.amortcar.utils.DatePickerFragment;
import com.project.dmitry.amortcar.utils.DatePickerFragmentListener;
import com.project.dmitry.amortcar.utils.MLog;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.Callable;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

public class StatisticActivity extends AppCompatActivity implements MultiSelectionSpinner.OnMultipleItemsSelectedListener, DatePickerFragmentListener {


    MultiSelectionSpinner carSpinner;

    List<Car> cars;
    List<Integer> selectedCars;
    private Calendar dateFrom;
    private Calendar dateTo;
    Disposable disposableMilage;
    Disposable disposablePlans;

    private String whereCarString;
    private String whereTimeString;

    private Single<List<Plan>> mPlanDateSingle;
    private Single<List<Milage>> mMilageDateSingle;
    private List<Plan>mDate;
    private List<Milage>mMilageDate;
    private ArrayList<MyDataListeners> listeners = new ArrayList<>(4);
    public void addDataListener(MyDataListeners listener){
        if(mDate!=null)listener.onPlansDataUpdate(mDate);
        listeners.add(listener);
    }
    public void removeDataListener(MyDataListeners listener){
        listeners.remove(listener);
    }


    private static final String DEFAULT_INPUT_TIME = "___________";

    @BindView(R.id.statistic_time_part_period_layout)
    LinearLayout timePartPeriodLayout;
    @BindView(R.id.statistic_time_part_sp)
    Spinner timePartSpinner;
    @BindView(R.id.statistic_time_part_from)
    TextView timePartFrom;
    @BindView(R.id.statistic_time_part_to)
    TextView timePartTo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_statistic);
        ButterKnife.bind(this);
        initCarsSpinner();
        whereCarString = "car=" + MySugarApp.getSelectedCar().getId();

        timePartSpinnerInit();

        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.YEAR, -1);
        whereTimeString = "end_date > " + calendar.getTimeInMillis();

        updateDate();
    }

    private void initCarsSpinner() {
        cars = Car.listAll(Car.class);
        List<String> carItems = new ArrayList<>(cars.size() + 1);
        carItems.add(0, getString(R.string.statistic_all_cars));
        for (Car c : cars) {
            carItems.add(c.getTitle());
        }
        carSpinner = findViewById(R.id.ac_statistic_cars_spin);
        carSpinner.setItems(carItems);
        carSpinner.setListener(this);
        carSpinner.setSelection(new String[]{MySugarApp.getSelectedCar().getTitle()});
    }

    private void timePartSpinnerInit() {
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        adapter.addAll(getString(R.string.statistic_for_all_time),
                getString(R.string.statistic_for_year),
                getString(R.string.statistic_for_month),
                getString(R.string.statistic_for_period));
        timePartSpinner.setAdapter(adapter);
        timePartSpinner.setSelection(1);
        timePartSpinner.setOnItemSelectedListener(
                new AdapterView.OnItemSelectedListener() {
                    int lastPosition = 1;

                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        Calendar calendar;
                        if (lastPosition != position) {
                            lastPosition = position;
                            switch (position) {
                                case 0:
                                    //За все время. Данные по машинам без ограничений

                                    timePartPeriodLayout.setVisibility(View.GONE);
                                    whereTimeString = null;
                                    updateDate();
                                    break;
                                case 1:
                                    //за год дефолт
                                    timePartPeriodLayout.setVisibility(View.GONE);
                                    calendar = Calendar.getInstance();
                                    calendar.add(Calendar.YEAR, -1);
                                    whereTimeString = "end_date > " + calendar.getTimeInMillis();
                                    updateDate();
                                    break;
                                case 2:
                                    timePartPeriodLayout.setVisibility(View.GONE);
                                    calendar = Calendar.getInstance();
                                    calendar.add(Calendar.MONTH, -1);
                                    whereTimeString = "end_date > " + calendar.getTimeInMillis();
                                    updateDate();
                                    break;
                                case 3:
                                    timePartPeriodLayout.setVisibility(View.VISIBLE);
                                    periodTimeInput();
                                    break;
                                default:
                                    break;
                            }
                        }
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {
                    }
                }
        );
    }

    private void updateDate() {
        //сообщаем всем, что грузимся
        //получаем данные из бд, рассылаем их всем слушателям.
        for (MyDataListeners l : listeners) {
            l.onRefreshingStart();
        }
        if (disposableMilage != null) {
            disposableMilage.dispose();
        }
        if (disposablePlans != null) {
            disposablePlans.dispose();
        }
        mPlanDateSingle = Single.fromCallable(new Callable<List<Plan>>() {
            @Override
            public List<Plan> call() throws Exception {
                String where = "("+whereCarString+")" + " and CLOSE_STATUS=1";
                if (whereTimeString != null) {
                    where = where + " and " + whereTimeString ;
                }
                return Plan.find(Plan.class, where, null);
            }
        }).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
        mMilageDateSingle = Single.fromCallable(new Callable<List<Milage>>() {
            @Override
            public List<Milage> call() throws Exception {
                String where = whereCarString;
                if (whereTimeString != null) {
                    where = where + " and " + whereTimeString.replaceAll("end_date","date");
                }
                return Milage.find(Milage.class, where, null);
            }
        }).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
        disposablePlans=mPlanDateSingle.subscribeWith(new DisposableSingleObserver<List<Plan>>() {
            @Override
            public void onSuccess(List<Plan> plans) {
                mDate = plans;
                MLog.log("THREADSS","onSuccess stat "+ Thread.currentThread().toString());
                if(plans!=null&&plans.size()==0){
                    Toast.makeText(StatisticActivity.this,getString(R.string.ac_statistic_empty_data),Toast.LENGTH_LONG).show();
                }
                for (MyDataListeners l : listeners) {
                    l.onPlansDataUpdate(mDate);
                }
                if (disposablePlans != null) {
                    disposablePlans.dispose();
                }
            }

            @Override
            public void onError(Throwable e) {
                for (MyDataListeners l : listeners) {
                    l.onPlanError();
                    if (disposablePlans != null) {
                        disposablePlans.dispose();
                    }
                }
            }
        });
        disposableMilage=mMilageDateSingle.subscribeWith(new DisposableSingleObserver<List<Milage>>() {
            @Override
            public void onSuccess(List<Milage> milages) {
                mMilageDate = milages;
                if(milages!=null&&milages.size()==0){
                    Toast.makeText(StatisticActivity.this,getString(R.string.ac_statistic_empty_data_milages),Toast.LENGTH_LONG).show();
                }
                for (MyDataListeners l : listeners) {
                    l.onMilageDataUpdate(mMilageDate);
                }
                if (disposableMilage != null) {
                    disposableMilage.dispose();
                }
            }

            @Override
            public void onError(Throwable e) {
                for (MyDataListeners l : listeners) {
                    l.onMilageError();
                    if (disposableMilage != null) {
                        disposableMilage.dispose();
                    }
                }
            }
        });
    }

    private void periodTimeInput() {
        timePartFrom.setText(DEFAULT_INPUT_TIME);
        timePartTo.setText(DEFAULT_INPUT_TIME);
        timePartFrom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerFragment newFragment = new DatePickerFragment();
                newFragment.setDatePickerListener(StatisticActivity.this);
                newFragment.show(StatisticActivity.this.getSupportFragmentManager(), "datePickerFrom");
            }
        });
        timePartTo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerFragment newFragment = new DatePickerFragment();
                newFragment.setDatePickerListener(StatisticActivity.this);
                newFragment.show(StatisticActivity.this.getSupportFragmentManager(), "datePickerTo");
            }
        });
    }

    public static void start(Context ctx) {
        Intent intent = new Intent(ctx, StatisticActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        ctx.startActivity(intent);
    }

    @Override
    public void selectedIndices(List<Integer> indices) {
        selectedCars = indices;
        StringBuilder sb = new StringBuilder();
        boolean foundOne = false;
        for (int i = 0; i < indices.size(); ++i) {
            if (foundOne) {
                sb.append(" or ");
            }
            int index =indices.get(i);
            if(index!=0) {
                sb.append("car=");
                long id = cars.get(index-1).getId();//no item @allCars@
                sb.append(id);
                foundOne = true;
            }
        }
        whereCarString = sb.toString();
        updateDate();
    }

    @Override
    public void onDateSet(int year, int month, int day, String tag) {
        if (tag == null) return;
        if (tag.equals("datePickerFrom")) {
            timePartFrom.setText(getResources().getString(R.string.date_text_template, day, month, year));
            dateFrom = Calendar.getInstance();
            dateFrom.set(year, month, day);
        }
        if (tag.equals("datePickerTo")) {
            timePartTo.setText(getResources().getString(R.string.date_text_template, day, month, year));
            dateTo = Calendar.getInstance();
            dateTo.set(year, month, day);
        }

        if (!timePartFrom.getText().equals(DEFAULT_INPUT_TIME) && !timePartTo.getText().equals(DEFAULT_INPUT_TIME)) {
            if (dateFrom != null && dateTo != null) {
                whereTimeString = "end_date between " + dateFrom.getTimeInMillis() + " and " + dateTo.getTimeInMillis();
                updateDate();
            }
        }
    }


    @Override
    public void selectedStrings(List<String> strings) {
        //do nothing
    }
}
