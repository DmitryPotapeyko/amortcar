package com.project.dmitry.amortcar.servicesPac;

import android.app.IntentService;
import android.content.Intent;
import android.content.Context;

import com.project.dmitry.amortcar.models.Car;
import com.project.dmitry.amortcar.models.Milage;
import com.project.dmitry.amortcar.R;

import java.util.Calendar;
import java.util.Iterator;
import java.util.List;

import static com.project.dmitry.amortcar.servicesPac.NotificationBroadcastReceiver.getNotification;
import static com.project.dmitry.amortcar.servicesPac.NotificationBroadcastReceiver.sendNotification;


public class NotificationService extends IntentService {
    private static final String ACTION_NOTIFICATION = "com.project.dmitry.amortcar.ServicesPac.action.NOTIFICATION";
    private static final int NOTIFICATION_ID_DIF = 12451;//случайное число


//    private static final String EXTRA_PARAM1 = "com.project.dmitry.amortcar.ServicesPac.extra.PARAM1";
//    private static final String EXTRA_PARAM2 = "com.project.dmitry.amortcar.ServicesPac.extra.PARAM2";

    public NotificationService() {
        super("NotificationService");
    }


    public static void startActionNotification(Context context) {
        Intent intent = new Intent(context, NotificationService.class);
        intent.setAction(ACTION_NOTIFICATION);
        context.startService(intent);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            final String action = intent.getAction();
            if (ACTION_NOTIFICATION.equals(action)) {
                handleActionNotification();
                stopSelf();
            }
        }
    }


    private void handleActionNotification() {
        List<Car> cars = Car.listAll(Car.class);
        Calendar today = Calendar.getInstance();
        Calendar weekAgo = Calendar.getInstance();
        weekAgo.add(Calendar.DAY_OF_MONTH, -7);
        for (Car car : cars) {
            Iterator<Milage> milages = Milage.findAsIterator(Milage.class, "car=?", new String[]{car.getId().toString()},
                    null,"date desc",null);
            //a week? but if no milages on this week - to first milages
            int mil = 0;
            Calendar date;
            Milage lastMilage, milage = null;
            if (milages.hasNext()) {
                lastMilage = milages.next();
                if (lastMilage.getDate().before(weekAgo)) {
                    //Notification за последние 7 дней вы проехали 0 км.
                    sendNotification(
                            getNotification(getApplicationContext(), car.getTitle(),
                                    getString(R.string.notif_7_days_0_km),
                                    getString(R.string.notif_should_be_save) +
                                            getResources().getString(R.string.money_double_text_template,lastMilage.getAccumulatedMoney(),"")
                                    ,true),
                            getApplicationContext(),
                            NOTIFICATION_ID_DIF + car.getId().intValue()
                    );
                    continue;
                }
            } else {
                sendNotification(
                        getNotification(getApplicationContext(), car.getTitle(),
                                getString(R.string.notif_no_milages),
                                getString(R.string.notif_no_milage_subtext),false),
                        getApplicationContext(),
                        NOTIFICATION_ID_DIF + car.getId().intValue()
                );
                continue;
            }

            while (milages.hasNext()) {
                milage = milages.next();
                if (milage.getDate().before(weekAgo)) {
                    break;
                }
            }

            if (milage == null) {//только одна запись была
//                Notification на  число пробег составляет 20001 км. Накопленно т руб
                sendNotification(
                        getNotification(getApplicationContext(), car.getTitle(),
                                getString(R.string.notif_na) + getResources().getString(R.string.date_text_template,
                                        lastMilage.getDate().get(Calendar.DAY_OF_MONTH),
                                        lastMilage.getDate().get(Calendar.MONTH),
                                        lastMilage.getDate().get(Calendar.YEAR)
                                ) + " пробег составлял " + lastMilage.getLength() + ".",
                                getString(R.string.notif_should_be_save) +
                                        getResources().getString(R.string.money_double_text_template,lastMilage.getAccumulatedMoney(),""),false),
                        getApplicationContext(),
                        NOTIFICATION_ID_DIF + car.getId().intValue()
                );
            } else {
                mil = lastMilage.getLength() - milage.getLength();
                sendNotification(
                        getNotification(getApplicationContext(), car.getTitle(),
                                getString(R.string.notif_c) + getResources().getString(R.string.date_text_template,
                                        milage.getDate().get(Calendar.DAY_OF_MONTH),
                                        milage.getDate().get(Calendar.MONTH),
                                        milage.getDate().get(Calendar.YEAR)) +
                                        getString(R.string.notif_po)+getResources().getString(R.string.date_text_template,
                                        lastMilage.getDate().get(Calendar.DAY_OF_MONTH),
                                        lastMilage.getDate().get(Calendar.MONTH),
                                        lastMilage.getDate().get(Calendar.YEAR))+
                                        getString(R.string.notif_milage_is) + mil + ".",
                                getString(R.string.notif_should_be_save)
                                        +  getResources().getString(R.string.money_double_text_template,lastMilage.getAccumulatedMoney(),""),false),
                        getApplicationContext(),
                        NOTIFICATION_ID_DIF + car.getId().intValue()
                );
            }
        }
    }
}
