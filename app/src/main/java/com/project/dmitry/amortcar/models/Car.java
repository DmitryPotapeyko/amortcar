package com.project.dmitry.amortcar.models;

import android.graphics.Bitmap;

import com.orm.SugarRecord;
import com.orm.dsl.Ignore;
import com.orm.dsl.Unique;
import com.project.dmitry.amortcar.models.Utils.DbBitmapUtility;


public final class Car extends SugarRecord{
    @Unique
    private long id;
    private int creationYear;
    private String brand;
    @Ignore
    private String title= null;

    public int getCreationYear() {
        return creationYear;
    }

    public String getBrand() {
        return brand;
    }

    public String getModel() {
        return model;
    }

    private String model;

    public String getName() {
        return name;
    }

    private String name;
    private byte[] photo;
    @Ignore
    private Bitmap bitmapPhoto;
    public Car() {
    }
    public void  setParametrs (int creationYear, String brand, String model, String name, Bitmap photo){
        this.creationYear = creationYear;
        this.brand = brand;
        this.model = model;
        this.name = name;
        this.bitmapPhoto = photo;
        if (bitmapPhoto != null) {
            this.photo = DbBitmapUtility.getBytes(photo);
        }
    }

    public Car(int creationYear, String brand, String model, String name, Bitmap photo) {
        this.creationYear = creationYear;
        this.brand = brand;
        this.model = model;
        this.name = name;
        this.bitmapPhoto = photo;
        if (bitmapPhoto != null) {
            this.photo = DbBitmapUtility.getBytes(photo);
        }
    }

    public String getTitle() {
        if(title!=null)return title;

        title = name;
        if (title == null || title.equals("")) {
            title = getBrand() + " " + getModel() + " (" + getCreationYear()+")";
        }
        if(title.length()>20){title = title.substring(0,18)+"...";}
        return title;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Car car = (Car) o;

        return id == car.id;
    }

    @Override
    public int hashCode() {
        return (int) (id ^ (id >>> 32));
    }
}