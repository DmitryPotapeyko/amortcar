package com.project.dmitry.amortcar.statisticsActivityPac;

import com.project.dmitry.amortcar.models.Milage;
import com.project.dmitry.amortcar.models.Plan;

import java.util.List;

interface MyDataListeners {
    void onPlansDataUpdate(List<Plan> data);

    void onMilageDataUpdate(List<Milage> data);
    void onRefreshingStart();
    void onPlanError();
    void onMilageError();
}
