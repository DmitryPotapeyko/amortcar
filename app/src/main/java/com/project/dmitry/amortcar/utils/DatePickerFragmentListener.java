package com.project.dmitry.amortcar.utils;

import android.support.annotation.Nullable;


public interface DatePickerFragmentListener {
    void onDateSet(int year, int month, int day, @Nullable String tag);
}
