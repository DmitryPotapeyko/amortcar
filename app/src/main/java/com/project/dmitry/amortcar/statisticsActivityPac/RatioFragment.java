package com.project.dmitry.amortcar.statisticsActivityPac;


import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.project.dmitry.amortcar.models.Milage;
import com.project.dmitry.amortcar.models.Plan;
import com.project.dmitry.amortcar.R;
import com.project.dmitry.amortcar.utils.MLog;

import java.util.ArrayList;
import java.util.List;

public class RatioFragment extends Fragment implements
        OnChartValueSelectedListener, MyDataListeners {

    private PieChart mChart;
    LinearLayout progressView;


    public RatioFragment() {
        // Required empty public constructor
    }

    public static RatioFragment newInstance() {
        return new RatioFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_ratio, container, false);
        progressView = view.findViewById(R.id.fr_ratio_progress_view);
        mChart = view.findViewById(R.id.fr_ratio_piechart);
        mChart.setUsePercentValues(true);
        mChart.getDescription().setEnabled(false);
        mChart.setExtraOffsets(5, 10, 5, 5);
        mChart.setDrawHoleEnabled(true);
        mChart.setHoleColor(Color.WHITE);
        mChart.setTransparentCircleColor(Color.WHITE);
        mChart.setTransparentCircleAlpha(110);
        mChart.setCenterText(getResources().getString(R.string.app_name));
        mChart.setDrawCenterText(true);
        mChart.setDragDecelerationFrictionCoef(0.95f);

        mChart.setRotationAngle(0);
        // enable rotation of the chart by touch
        mChart.setRotationEnabled(true);
        mChart.setHighlightPerTapEnabled(true);
        setData(2, 100);
        Legend l = mChart.getLegend();
        l.setVerticalAlignment(Legend.LegendVerticalAlignment.TOP);
        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.RIGHT);
        l.setOrientation(Legend.LegendOrientation.VERTICAL);
        l.setDrawInside(false);
        l.setXEntrySpace(7f);
        l.setYEntrySpace(0f);
        l.setYOffset(0f);

        // entry label styling
        mChart.setEntryLabelColor(Color.WHITE);
        mChart.setEntryLabelTextSize(12f);


        progressView.setVisibility(View.VISIBLE);
        ((StatisticActivity) getActivity()).addDataListener(this);
        mChart.setOnChartValueSelectedListener(this);
        return view;
    }

    private void setData(double planed, double notPlaned) {
        ArrayList<PieEntry> entries = new ArrayList<>();
        // NOTE: The order of the entries when being added to the entries array determines their position around the center of
        // the chart.

        entries.add(new PieEntry((float) planed, getString(R.string.statistic_ratio_planned), planed));
        entries.add(new PieEntry((float) notPlaned, getString(R.string.statistic_ratio_not_planed), notPlaned));

        PieDataSet dataSet = new PieDataSet(entries, "");

        dataSet.setDrawIcons(false);

        dataSet.setSliceSpace(3f);
        dataSet.setSelectionShift(10f);

        // add a lot of colors

        String[] colors = getResources().getStringArray(R.array.bar_colors);
        int[] col = new int[colors.length];
        int l = 0;
        for (String c : colors) {
            col[l++] = Color.parseColor(c);
        }

        dataSet.setColors(col);
        //dataSet.setSelectionShift(0f);

        PieData data = new PieData(dataSet);
        data.setValueFormatter(new PercentFormatter());
        data.setValueTextSize(11f);
        data.setValueTextColor(Color.WHITE);
        if (planed == notPlaned && planed == 0) {
            mChart.setData(null);
            mChart.setNoDataText(getString(R.string.not_enough_data));
            mChart.setNoDataTextColor(Color.BLACK);
        } else {
            mChart.setData(data);
        }

        // undo all highlights
        mChart.highlightValues(null);
        mChart.setDrawMarkers(false);//фикс баг при перезагрузке данных, небыло сброса маркера, и если он указывал на отсутствующий столбик. то nullPointer
        mChart.invalidate();
        mChart.animateXY(750, 750);
    }

    void updateRatioFragment(@NonNull List<Plan> mData) {
        double planed = 0;
        double notPlaned = 0;
        for (Plan plan : mData) {
            if (plan.getEndDate().equals(plan.getStartDate())) {
                notPlaned += plan.getPrice();
            } else {
                planed += plan.getPrice();
            }
        }
        setData(planed, notPlaned);
    }

    @Override
    public void onDestroyView() {
        ((StatisticActivity) getActivity()).removeDataListener(this);
        super.onDestroyView();
    }

    @Override
    public void onValueSelected(Entry e, Highlight h) {
        mChart.setDrawMarkers(true);//фикс а эта штука на первую отрисовку убирает маркер, поэтому падения нет
        mChart.setCenterText(getResources().getString(R.string.money_double_text_template, e.getData(),
                getResources().getString(R.string.ruble)));//todo
    }

    @Override
    public void onNothingSelected() {
        mChart.setCenterText(getResources().getString(R.string.app_name));
    }

    @Override
    public void onPlansDataUpdate(List<Plan> data) {
        MLog.log("THREADSS", "onPlansDataUpdate " + Thread.currentThread().toString());
        progressView.setVisibility(View.GONE);
        mChart.setVisibility(View.VISIBLE);
        if (data != null) {
            updateRatioFragment(data);
        }
    }

    @Override
    public void onMilageDataUpdate(List<Milage> data) {

    }

    @Override
    public void onRefreshingStart() {
        MLog.log("THREADSS", "onRefreshingStart " + Thread.currentThread().toString());
        if (progressView != null)
            progressView.setVisibility(View.VISIBLE);
        mChart.setVisibility(View.INVISIBLE);
    }

    @Override
    public void onPlanError() {
        progressView.setVisibility(View.GONE);
        mChart.setVisibility(View.VISIBLE);
        Toast.makeText(getContext(), "Произошла ошибка при загрузке данных", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onMilageError() {

    }
}

