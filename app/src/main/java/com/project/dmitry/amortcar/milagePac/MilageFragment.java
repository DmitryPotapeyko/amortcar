package com.project.dmitry.amortcar.milagePac;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.project.dmitry.amortcar.MySugarApp;
import com.project.dmitry.amortcar.R;
import com.project.dmitry.amortcar.models.Car;
import com.project.dmitry.amortcar.models.Milage;
import com.project.dmitry.amortcar.models.Plan;
import com.project.dmitry.amortcar.utils.InputFormatException;
import com.project.dmitry.amortcar.utils.InputValidator;

import java.util.Calendar;
import java.util.List;


public class MilageFragment extends DialogFragment {
    private double accumulatedMoney = 0;

    public interface ResultListener {
        void onResult();
    }

    ResultListener listener;

    public void setAccumulatedMoney(double accumulatedMoney) {
        if (accumulatedMoney < 0) this.accumulatedMoney = 0;
        else this.accumulatedMoney = accumulatedMoney;
    }

    public void setResultListener(ResultListener listener) {
        this.listener = listener;
    }

    private static String CAR_ID_EXTRA = "CAR_ID_EXTRA";
    private int milage;
    private Car selectedCar;
    private EditText milageInput;
    TextInputLayout milageInputLayout;
    private TextView dateTextView;
    private Calendar calendar = Calendar.getInstance();
    private LinearLayout questLayout;
    private Button saveBtn;
    private int newMilage = -1;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        selectedCar = MySugarApp.getSelectedCar();
        if (selectedCar == null) {
            throw new IllegalStateException("no car");
        }

        View v = inflater.inflate(R.layout.fragment_milage, container);
        milageInput = v.findViewById(R.id.fr_milage_input);
        milageInputLayout = v.findViewById(R.id.fr_milage_input_textinputlayout);
        saveBtn = v.findViewById(R.id.fr_milage_save_btn);
        Button canselBtn = v.findViewById(R.id.fr_milage_cancel_btn);
        Button okBtn = v.findViewById(R.id.fr_milage_ok_btn);
        questLayout = v.findViewById(R.id.fr_milage_quest_layout);
        dateTextView = v.findViewById(R.id.fr_milage_date);
        dateTextView.setText(getResources().getString(R.string.date_text_template, calendar.get(Calendar.DAY_OF_MONTH),
                calendar.get(Calendar.MONTH), calendar.get(Calendar.YEAR)));

        if (MySugarApp.getLastMilageOfSelectedCar() != null) {
            milage = MySugarApp.getLastMilageOfSelectedCar().getLength();
        } else {
            milage = 0;
        }
        milageInput.setText(String.valueOf(milage));

        saveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                saveBtnClicked();
            }
        });
        canselBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                milageInput.setEnabled(true);
                questLayout.setVisibility(View.GONE);
                saveBtn.setVisibility(View.VISIBLE);
            }
        });

        okBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                saveMilage(newMilage);
            }
        });
        return v;
    }

    private void saveBtnClicked() {
        milageInputLayout.setErrorEnabled(false);
        try {
            newMilage = InputValidator.integerDecode(milageInput.getText().toString());
        } catch (InputFormatException e) {
            milageInputLayout.setError("Ошибка формата");
            return;
        }
        if (newMilage < milage) {
            milageInput.setEnabled(false);
            saveBtn.setVisibility(View.GONE);
            questLayout.setVisibility(View.VISIBLE);
        } else {
            saveMilage(newMilage);
        }
    }

    private void saveMilage(final int newMilage) {
        final List<Plan> plans = Plan.find(Plan.class, "car=? and CLOSE_STATUS=?",
                String.valueOf(selectedCar.getId()), "0");
        double money = 0;
        for(Plan p :plans){
            money+=p.getMoneyWithoutUpdateIn(newMilage,Calendar.getInstance());
        }

        Milage newMilageObj = new Milage(calendar, newMilage, Car.findById(Car.class, selectedCar.getId()), money);
        newMilageObj.save();
        MySugarApp.setLastMilageOfSelectedCar(newMilageObj);

        // SQLite does not have a separate Boolean storage class. Instead,
        // Boolean values are stored as integers 0 (false) and 1 (true).
        for(Plan p :plans){
            p.updatePassedParametrs(newMilageObj,Calendar.getInstance());
        }
        if (listener != null) {
            listener.onResult();
        }
        dismiss();
    }
}
