package com.project.dmitry.amortcar.carActivityPac;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


import com.project.dmitry.amortcar.mainActivityPac.MainActivity;
import com.project.dmitry.amortcar.models.Car;
import com.project.dmitry.amortcar.models.Milage;
import com.project.dmitry.amortcar.models.Plan;
import com.project.dmitry.amortcar.MySugarApp;
import com.project.dmitry.amortcar.R;
import com.project.dmitry.amortcar.utils.InputValidator;
import com.project.dmitry.amortcar.utils.InputFormatException;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Iterator;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AddCarActivity extends AppCompatActivity {
    private static final String TAG = "AppCompatActivity";
    @BindView(R.id.brand_input)
    EditText brandInput;
    @BindView(R.id.model_input)
    EditText modelInput;
    @BindView(R.id.year_input)
    EditText yearInput;
    @BindView(R.id.milage_input)
    EditText milageInput;
    @BindView(R.id.name_input)
    EditText nameInput;
    @BindView(R.id.saveBtn)
    Button saveBtn;
    @BindView(R.id.ac_add_car_deleteBtn)
    Button deleteBtn;
    @BindView(R.id.brand_input_layout)
    TextInputLayout brandLayout;
    @BindView(R.id.model_input_layout)
    TextInputLayout modelLayout;
    @BindView(R.id.year_input_layout)
    TextInputLayout yearLayout;
    @BindView(R.id.milage_input_layout)
    TextInputLayout milageLayout;
    @BindView(R.id.name_input_layout)
    TextInputLayout nameLayout;
    Boolean isEditMode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_car);
        ButterKnife.bind(this);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setHomeButtonEnabled(true);
        }
        isEditMode = false;
        Intent intent = getIntent();
        if (intent != null && MySugarApp.getSelectedCar() != null) {
            isEditMode = intent.getBooleanExtra(TAG, false);
        }

        if (isEditMode) {
            milageLayout.setVisibility(View.GONE);
            deleteBtn.setVisibility(View.VISIBLE);
            deleteBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    deleteBtnClicked();
                }
            });
            Car c = MySugarApp.getSelectedCar();
            modelInput.setText(c.getModel());
            nameInput.setText(c.getName());
            yearInput.setText(String.valueOf(c.getCreationYear()));
            brandInput.setText(c.getBrand());
        } else {
            milageLayout.setVisibility(View.VISIBLE);
            deleteBtn.setVisibility(View.GONE);
        }

        saveBtn.setOnClickListener(
                new View.OnClickListener() {
                    int currentYear = new GregorianCalendar().get(Calendar.YEAR);
                    String model;
                    String brand;
                    String name;
                    String year;
                    String milage;
                    Integer yearPars = null;
                    Integer milagePars = null;
                    boolean isErrorInput = false;

                    @Override
                    public void onClick(View view) {
                        yearPars = null;
                        isErrorInput = false;
                        model = modelInput.getText().toString();
                        brand = brandInput.getText().toString();
                        name = nameInput.getText().toString();
                        milage = milageInput.getText().toString();
                        year = yearInput.getText().toString();
                        brandLayout.setErrorEnabled(false);
                        modelLayout.setErrorEnabled(false);
                        yearLayout.setErrorEnabled(false);
                        milageLayout.setErrorEnabled(false);

                        if (brand.equals("")) {
                            brandLayout.setError(getString(R.string.empty_required_field));
                            isErrorInput = true;
                        }
                        if (model.equals("")) {
                            modelLayout.setError(getString(R.string.empty_required_field));
                            isErrorInput = true;
                        }
                        if (year.equals("")) {
                            yearLayout.setError(getString(R.string.empty_required_field));
                            isErrorInput = true;
                        } else {
                            try {
                                yearPars = InputValidator.integerDecode(year);
                            } catch (InputFormatException e) {
                                yearLayout.setError(getString(R.string.format_error_text));
                                isErrorInput = true;
                            }
                            if (yearPars < 1806 || yearPars > currentYear) {
                                yearLayout.setError(getString(R.string.year_rang_error_text) + currentYear);
                                isErrorInput = true;
                            }
                        }
                        if (!isEditMode) {
                            if (milage.equals("")) {
                                milageLayout.setError(getString(R.string.empty_required_field));
                                isErrorInput = true;
                            } else {
                                try {
                                    milagePars = InputValidator.integerDecode(milage);
                                } catch (InputFormatException e) {
                                    milageLayout.setError(getString(R.string.format_error_text));
                                    isErrorInput = true;
                                }
                            }
                        }
                        if (!isErrorInput) {
                            Car newCar;
                            if (!isEditMode) {
                                newCar = new Car(yearPars, brand, model, name, null);
                                newCar.save();
                                Milage firstMilage = new Milage(Calendar.getInstance(), milagePars, newCar, 0);
                                firstMilage.save();
                                MySugarApp.setLastMilageOfSelectedCar(firstMilage);
                            } else {
                                newCar = MySugarApp.getSelectedCar();
                                newCar.setParametrs(yearPars, brand, model, name, null);
                                newCar.update();
                            }
                            MySugarApp.setSelectedCar(newCar);
                            MainActivity.start(AddCarActivity.this);
                        }
                    }
                }
        );
    }

    private void deleteBtnClicked() {
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setTitle(R.string.delete_auto_dialog_title);
        dialog.setMessage(R.string.delete_auto_dialog_text);
        dialog.setNegativeButton(R.string.cancel_button, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        dialog.setPositiveButton(R.string.delete_button, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Car c = MySugarApp.getSelectedCar();
                Iterator<Plan> plans = Plan.findAsIterator(Plan.class, "car=?", String.valueOf(c.getId()));
                Iterator<Milage> milages = Milage.findAsIterator(Milage.class, "car=?", String.valueOf(c.getId()));
                MySugarApp.setSelectedCar(null);
                MySugarApp.setLastMilageOfSelectedCar(null);
                while (plans.hasNext()) {
                    plans.next().delete();
                }
                while (milages.hasNext()) {
                    milages.next().delete();
                }
                c.delete();
                Toast.makeText(getApplicationContext(), R.string.auto_deleted, Toast.LENGTH_LONG).show();
                MainActivity.start(AddCarActivity.this);
            }
        });
        dialog.show();
    }

    public static void start(Context ctx) {
        Intent intent = new Intent(ctx, AddCarActivity.class);
        ctx.startActivity(intent);
    }

    public static void start(Context ctx, boolean isEdit) {
        Intent intent = new Intent(ctx, AddCarActivity.class);
        if (isEdit) {
            intent.putExtra(TAG, true);
        }
        ctx.startActivity(intent);
    }
}
