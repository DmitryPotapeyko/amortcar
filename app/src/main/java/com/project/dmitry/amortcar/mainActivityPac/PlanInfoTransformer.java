package com.project.dmitry.amortcar.mainActivityPac;

import com.project.dmitry.amortcar.models.Car;
import com.project.dmitry.amortcar.models.Type;

import java.io.Serializable;
import java.util.Calendar;


public class PlanInfoTransformer implements Serializable {
    private Long car;
    private String typeName;
    private double price;
    private Calendar startDate;
    private Integer lengthPeriod;
    private Calendar endDate;


    public PlanInfoTransformer(Car car, Type type, double price, Calendar startDate, Calendar endDate, Integer lengthPeriod) {
        this.car = car.getId();
        this.typeName = type.getName();
        this.price = price;
        this.startDate = startDate;
        this.lengthPeriod = lengthPeriod;
        this.endDate = endDate;

    }

    public Long getCarId() {
        return car;
    }

    public String getTypeName() {
        return typeName;
    }

    public double getPrice() {
        return price;
    }

    public Calendar getStartDate() {
        return startDate;
    }

    public Integer getLengthPeriod() {
        return lengthPeriod;
    }

    public Calendar getEndDate() {
        return endDate;
    }
}
