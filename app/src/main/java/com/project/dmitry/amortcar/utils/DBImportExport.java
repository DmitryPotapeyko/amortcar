package com.project.dmitry.amortcar.utils;

import android.Manifest;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.project.dmitry.amortcar.R;
import com.project.dmitry.amortcar.startActivity.WelcomeActivity;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DBImportExport extends AppCompatActivity {
    private static final int MY_PERMISSIONS_REQUEST_CODE = 6723;
    @BindView(R.id.button_export)
    Button exportB;
    @BindView(R.id.button_import)
    Button importB;
    @BindView(R.id.ac_dbimport_export_text1)
    TextView text1;
    @BindView(R.id.ac_dbimport_export_text2)
    TextView text2;
    private File sdFile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dbimport_export);
        ButterKnife.bind(this);
        //работа с разрешениями
        if (Build.VERSION.SDK_INT > 22) {
            if (ContextCompat.checkSelfPermission(DBImportExport.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED) {
                // Permission is not granted
                // No explanation needed; request the permission
                ActivityCompat.requestPermissions(DBImportExport.this,
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        MY_PERMISSIONS_REQUEST_CODE);
            }
        }

        importB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ContextCompat.checkSelfPermission(DBImportExport.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        == PackageManager.PERMISSION_GRANTED) {
                    importDB();
                } else {
                    showPermissionDialog();
                }
            }
        });
        exportB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ContextCompat.checkSelfPermission(DBImportExport.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        == PackageManager.PERMISSION_GRANTED) {
                    exportDB();
                } else {
                    showPermissionDialog();
                }
            }
        });

    }

    private void showPermissionDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.permission_dialog_message)
                .setTitle(R.string.permission_dialog_title);
        builder.setPositiveButton(R.string.permission_dialog_to_settings, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                goToSettings();
            }
        });
        builder.setNegativeButton(R.string.permission_dialog_cancle, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                //ничего
            }
        });
        builder.create().show();
    }

    private void goToSettings() {
        Intent myAppSettings = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS, Uri.parse("package:" + getPackageName()));
        myAppSettings.addCategory(Intent.CATEGORY_DEFAULT);
        myAppSettings.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(myAppSettings);
    }

    //importing database
    private void importDB() {
        try {
            //creating a new folder for the database to be backuped to
            File direct = Environment.getExternalStoragePublicDirectory("AMORTCAR");
            // добавляем свой каталог к пути
            direct = new File(direct.getAbsolutePath());
            // создаем каталог
            direct.mkdirs();
            // формируем объект File, который содержит путь к файлу
            sdFile = new File(direct, "amortcar.db");

            direct = this.getDatabasePath("AmortcarSugar.db");
            // открываем поток для записи
            FileInputStream in = new FileInputStream(sdFile);
            FileChannel inCh = in.getChannel();
            boolean delRes = direct.delete();
            FileOutputStream out = new FileOutputStream(direct);
            FileChannel outCh = out.getChannel();
            long len = sdFile.length();
            // пишем данные
            long outLen = inCh.transferTo(0, len, outCh);
            if (len == outLen) {
                Log.d("LOG_TAG", "Файл записан на SD: " + sdFile.getAbsolutePath());
            }
            inCh.close();
            outCh.close();
            text1.setVisibility(View.INVISIBLE);
            exportB.setVisibility(View.INVISIBLE);
            text2.setText(R.string.after_import_text);
            importB.setText(R.string.restart_application);
            importB.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    restartApp();
                }
            });
            Toast.makeText(this, this.getString(R.string.import_success), Toast.LENGTH_LONG).show();
        } catch (Exception e) {
            Toast.makeText(this, "Error " + e.getClass(), Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }
    }

    private void restartApp() {
        try {
            //create the intent with the default start activity for your application
            Intent mStartActivity = new Intent(getApplicationContext(), WelcomeActivity.class);
            mStartActivity.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            //create a pending intent so the application is restarted after System.exit(0) was called.
            // We use an AlarmManager to call this intent in 100ms
            int mPendingIntentId = 223344;
            PendingIntent mPendingIntent = PendingIntent
                    .getActivity(this, mPendingIntentId, mStartActivity,
                            PendingIntent.FLAG_CANCEL_CURRENT);
            AlarmManager mgr = (AlarmManager) this.getSystemService(Context.ALARM_SERVICE);
            if(mgr != null){
                mgr.set(AlarmManager.RTC, System.currentTimeMillis() + 10, mPendingIntent);}
            //kill the application
            finishAffinity();
            System.exit(0);
        } catch (Exception ex) {
            Toast.makeText(this, "Was not able to restart application", Toast.LENGTH_SHORT).show();
        }

    }

    //exporting database
    private void exportDB() {
        try {
            //creating a new folder for the database to be backuped to
            File direct = Environment.getExternalStoragePublicDirectory("AMORTCAR");
            // добавляем свой каталог к пути
            direct = new File(direct.getAbsolutePath());
            // создаем каталог
            direct.mkdirs();
            // формируем объект File, который содержит путь к файлу
            sdFile = new File(direct, "amortcar.db");

            direct = this.getDatabasePath("AmortcarSugar.db");
            // открываем поток для записи
            FileInputStream in = new FileInputStream(direct);
            FileChannel inCh = in.getChannel();
            FileOutputStream out = new FileOutputStream(sdFile);
            FileChannel outCh = out.getChannel();
            long len = direct.length();
            // пишем данные
            long outLen = inCh.transferTo(0, len, outCh);
            if (len == outLen) {
                Log.d("LOG_TAG", "Файл записан на SD: " + sdFile.getAbsolutePath());
            }
            inCh.close();
            outCh.close();
            Toast.makeText(this, this.getString(R.string.export_success), Toast.LENGTH_LONG).show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void start(Activity activity) {
        Intent intent = new Intent(activity, DBImportExport.class);
        activity.startActivity(intent);
    }
}

