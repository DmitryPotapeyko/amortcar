package com.project.dmitry.amortcar;

import android.content.SharedPreferences;
import android.content.res.Resources;
import android.preference.PreferenceManager;
import android.util.TypedValue;

import com.orm.SugarApp;
import com.project.dmitry.amortcar.models.Car;
import com.project.dmitry.amortcar.models.Milage;
import com.project.dmitry.amortcar.servicesPac.NotificationBroadcastReceiver;

import java.util.Calendar;
import java.util.HashMap;

public class MySugarApp extends SugarApp  implements SharedPreferences.OnSharedPreferenceChangeListener{
    private static final String periodKey = "period_list";
    private static final String timeKey = "notification_time";
    private static final String dayKey = "day_list";
    private static HashMap<String,Long> lastCommercial=new HashMap<>();

    /**
     *  проверка, когда последний раз показывали.
     * @return Если меньше 5 минут назад, то false, иначе true
     * возврат true приводит к записи запоминанию времени показа.
     * перезапуск ПРИЛОЖЕНИЯ (не активности) обнуляет время
     */
    public static boolean isTimeToShowCommercial(String key){
        long time = Calendar.getInstance().getTimeInMillis();
        int FIVE_MIN_IN_MILLIS = 5 * 60 * 1000;
        Long lastTime =lastCommercial.get(key);
        lastTime = lastTime==null?0:lastTime;
        if(time - lastTime> FIVE_MIN_IN_MILLIS){
            lastCommercial.put(key,time);
            return true;
        }
        return false;
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        if(periodKey.equals(key)||timeKey.equals(key)||dayKey.equals(key)){
            NotificationBroadcastReceiver.onNotificationSettingsChanged(getApplicationContext());
        }
    }
    public MySugarApp() {
        super();
    }

    static MySugarApp instance;

    static Car selectedCar;

    public static Milage getLastMilageOfSelectedCar() {
        return lastMilageOfSelectedCar;
    }

    public static void setLastMilageOfSelectedCar(Milage lastMilageOfSelectedCar) {
        MySugarApp.lastMilageOfSelectedCar = lastMilageOfSelectedCar;
    }

    static Milage lastMilageOfSelectedCar;

    public static void setSelectedCar(Car selectedCar) {
        MySugarApp.selectedCar = selectedCar;
    }

    public static Car getSelectedCar() {
        return selectedCar;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        PreferenceManager.setDefaultValues(this, R.xml.pref_general, false);
        PreferenceManager.getDefaultSharedPreferences(this).registerOnSharedPreferenceChangeListener(this);
        NotificationBroadcastReceiver.onAppStart(this);


    }


    public static Resources getApplicationResources() {
        if (instance == null) throw new IllegalStateException("Application context is not defined");
        return instance.getResources();
    }

    public static int getPixelsFromDPs(int dps) {
        if (instance == null) throw new IllegalStateException("Application context is not defined");
        return (int) (TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP, dps, instance.getResources().getDisplayMetrics()));
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        instance = null;
    }
}
